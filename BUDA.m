function varargout = BUDA(varargin)
% BUDA M-file for BUDA.fig
%      BUDA, by itself, creates a new BUDA or raises the existing
%      singleton*.
%
%      H = BUDA returns the handle to a new BUDA or the handle to
%      the existing singleton*.
%
%      BUDA('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in BUDA.M with the given input
%      arguments.
%
%      BUDA('Property','Value',...) creates a new BUDA or raises the
%      existing singleton*.  Starting from the left, property value pairs
%      are
%      applied to the GUI before BUDA_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to BUDA_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help BUDA

% Last Modified by GUIDE v2.5 19-Mar-2013 14:21:30

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @BUDA_OpeningFcn, ...
                   'gui_OutputFcn',  @BUDA_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT



% --- Executes just before BUDA is made visible.
function BUDA_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to BUDA (see VARARGIN)

% Choose default command line output for BUDA
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes BUDA wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = BUDA_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --------------------------------------------------------------------
function File_Callback(hObject, eventdata, handles)
% hObject    handle to File (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Action_Callback(hObject, eventdata, handles)
% hObject    handle to Action (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Help_Callback(hObject, eventdata, handles)
% hObject    handle to Help (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Open_Callback(hObject, eventdata, handles)
% hObject    handle to Open (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


uipushtool5_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Print_Callback(hObject, eventdata, handles)
% hObject    handle to Print (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Quit_Callback(hObject, eventdata, handles)
% hObject    handle to Quit (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

delete(handles.figure1);

% --------------------------------------------------------------------
function Untitled_4_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function About_Callback(hObject, eventdata, handles)
% hObject    handle to About (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Raw_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Raw_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Raw_signal_rb,'Value',1);
Raw_signal_rb_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Processed_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Processed_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Proc_signal_rb,'Value',1);
Proc_signal_rb_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Statistic_menu_Callback(hObject, eventdata, handles)
% hObject    handle to Statistic_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Stat_rb,'Value',1);
Stat_rb_Callback(hObject, eventdata, handles)


% --- Executes when figure1 is resized.
function figure1_ResizeFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes during object creation, after setting all properties.
function Raw_signal_rb_CreateFcn(hObject, eventdata, handles)
% hObject    handle to Raw_signal_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on button press in Change_seq.
function Change_seq_Callback(hObject, eventdata, handles)
% hObject    handle to Change_seq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function uipushtool5_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
     
     set(handles.pushbutton4,'Visible','off');
     set(handles.pushbutton5,'Visible','off');
     set(handles.Opening_pan,'Visible','off');
     set(handles.Raw_pan,'Visible','off');
     set(handles.Print,'Enable','on');
     set(handles.statistic_pan,'Visible','off');
     set(handles.processed_pan,'Visible','off');   
     set(handles.Selection_pan,'Visible','off');
     set(handles.filter_pan,'Visible','off');
     set(handles.Proc_signal_rb,'Value',0);
     set(handles.Raw_signal_rb,'Value',0);
     set(handles.Stat_rb,'Value',0);
     set(handles.uipushtool7,'Enable','off');
     set(handles.Raw_menu,'Enable','off');
     set(handles.Processed_menu,'Enable','off');
     set(handles.Statistic,'Enable','off');
     set(handles.Print,'Enable','off');
     set(handles.Raw_menu,'Enable','off');
     set(handles.Processed_menu,'Enable','off');
     set(handles.Statistic_menu,'Enable','off');
     set(handles.uipushtool12,'Enable','off');
     set(handles.uipushtool13,'Enable','off');
     set(handles.uipushtool14,'Enable','off');
     set(handles.changeseq,'Enable','off');
     set(handles.Untitled_7,'Enable','off');
     set(handles.Stat_rb,'Enable','off');
     set(handles.uitoggletool1,'Enable','on');
     set(handles.uitoggletool2,'Enable','on');
     set(handles.uitoggletool3,'Enable','on');
     

[file1,path]=uigetfile('*.gaz','Select .gaz file');
if file1==0
   
else
% 
     handles.gon_all=[];
 handles.gon_all=[];
    handles.acc_all=[];
cla(handles.R_GRF);
cla(handles.L_GRF);
handles.fsr=[];
handles.acc=[];
file=[];
handles.file=[path file1]; 
maxsamples=20000;
handles.flpf=0.5;
handles.fsample=1000/6;

handles.color3(1,:)=[1,1,0];
handles.color3(2,:)=[0,0,1];
handles.color3(3,:)=[1,0,0];
handles.color3(4,:)=[0,1,1];
handles.color3(5,:)=[0,1,0];
handles.color3(6,:)=[1,0,1];
handles.color3(7,:)=[0,0.5,1];
handles.color3(8,:)=[0.5,0.5,0.5];
handles.color3(9,:)=[0.5,0,0];
handles.color3(10,:)=[1,0.5,0.5];
handles.color3(11,:)=[0.5,1,0.5];
handles.color3(12,:)=[0,0,0.5];
handles.color3(13,:)=[0,0.5,0.5];
handles.color3(14,:)=[1,0.25,0];
handles.color3(15,:)=[0.25,0,1];
handles.color3(16,:)=[1,0.25,0.25];

handles.kk=0.9;
handles.nfsr=4;


n_sensor_channels=34;

fid=fopen(handles.file,'r');
status=fseek(fid,2400,'bof'); % 2400 bytes is reserved for comments
[handles.chdata,counter]=fread(fid,[n_sensor_channels maxsamples],'uint16');
handles.nsamples=counter/n_sensor_channels;    % number of samples in the file
fclose(fid);

set(handles.Selection_pan,'Visible','on');
set(handles.Opening_pan,'Visible','on');
set(handles.uipushtool7,'Enable','on');
set(handles.Print,'Enable','on');
set(handles.Raw_menu,'Enable','on');
set(handles.Processed_menu,'Enable','on');
set(handles.Raw,'Enable','on');
set(handles.Processed,'Enable','on');


handles.tstep=1/handles.fsample ;    % sampling period for buda

handles.timeall=0:1:handles.nsamples-1;
handles.timeall=handles.timeall*handles.tstep ;


fsrmin=0;
fsrmax=800;




for j=1:8
  fsr_all(j,:)=handles.chdata(j,:);
end

handles.accmax=24;

for i=1:handles.accmax
  handles.acc_all(i,:)=handles.chdata(8+i,:);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%


handles.g=9.806;        % gravitational acceleration
handles.length3=0.076;   % distance between ACCs on a rod

filec='calibration_acc_24.dat';
fidc=fopen(filec,'r');
load_datac=fscanf(fidc,'%g %g',[2 24]);
fclose(fidc);

for i=1:handles.accmax
    accmean(i)=load_datac(1,i);
    accgequ(i)=load_datac(2,i);
end

for i=1:handles.accmax
    handles.acc_all(i,:)=handles.acc_all(i,:)-accmean(i);
    handles.acc_all(i,:)=handles.acc_all(i,:)*handles.g/accgequ(i);
end


handles.gon_all(1,:)=handles.chdata(21,:);   %      KNEE R
handles.gon_all(2,:)=handles.chdata(22,:);   %      ANKLE R

handles.gon_all(2,:)=handles.gon_all(2,:)-mean(handles.gon_all(2,:));

for i=1:2
    handles.gon_all(i,:)=((handles.gon_all(i,:)-500)*90/227);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
axes(handles.FSR_R_O);

for j=1:handles.nfsr
    plot(handles.timeall,fsr_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.FSR_L_O);

for j=5:8
    plot(handles.timeall,fsr_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.ACC_R_O);

for j=1:handles.accmax/2
    plot(handles.timeall,handles.acc_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.ACC_L_O);

for j=handles.accmax/2+1:handles.accmax
    plot(handles.timeall,handles.acc_all(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

[xmark,ymark]=ginput(2);

set(handles.uipushtool12,'Enable','on');
set(handles.changeseq,'Enable','on');


handles.tstarts=xmark(1);
handles.tstops=xmark(2);

if handles.tstarts > handles.tstops
    temps=handles.tstarts;
    handles.tstarts=handles.tstops;
    handles.tstops=temps;
elseif handles.tstarts == handles.tstops
    handles.tstops=handles.tstarts+0.1;
end

handles.istart=round(handles.tstarts/handles.tstep+1);
handles.istop=round(handles.tstops/handles.tstep+1);
if handles.istart < 1
    handles.istart=1;
end
if handles.istop > handles.nsamples
    handles.istop=handles.nsamples;
end
handles.tstarts=(handles.istart-1)*handles.tstep;
handles.tstops=(handles.istop-1)*handles.tstep;
handles.nwork=handles.istop-handles.istart+1;

handles.time=0:1:handles.nwork-1;
handles.time=handles.time*handles.tstep;

for i=1:8
    handles.fsr(i,:)=fsr_all(i,handles.istart:handles.istop);
end


handles.fsr(4,:)=handles.fsr(4,:)-50;
handles.fsrx=handles.fsr;

for i=1:handles.accmax
    handles.acc(i,:)=handles.acc_all(i,handles.istart:handles.istop);
end

handles.accx=handles.acc;



axes(handles.FSR_R_O);

for j=1:handles.nfsr
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.FSR_L_O);

for j=5:8
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_R_O);

for j=1:handles.accmax/2
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_L_O);

for j=(handles.accmax/2+1):handles.accmax
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

end
guidata(hObject,handles)

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

handles.fsr=[];
handles.acc=[];
handles.kk=0.9;
handles.nfsr=4;
maxsamples=20000;
handles.flpf=0.5;
handles.fsample=1000/6;

n_sensor_channels=34;

fid=fopen(handles.file,'r');
status=fseek(fid,2400,'bof'); % 2400 bytes is reserved for comments
[handles.chdata,counter]=fread(fid,[n_sensor_channels maxsamples],'uint16');
handles.nsamples=counter/n_sensor_channels;    % number of samples in the file
fclose(fid);

set(handles.pushbutton4,'Visible','off');
set(handles.pushbutton5,'Visible','off');
set(handles.Selection_pan,'Visible','on');
set(handles.Print,'Enable','on');
set(handles.Opening_pan,'Visible','on');
set(handles.uitoggletool1,'Enable','on');
set(handles.uitoggletool2,'Enable','on');
set(handles.uitoggletool3,'Enable','on');

handles.tstep=1/handles.fsample;   % sampling period for buda

handles.timeall=0:1:handles.nsamples-1;
handles.timeall=handles.timeall*handles.tstep ;


fsrmin=0;
fsrmax=800;

for j=1:8
  fsr_all(j,:)=handles.chdata(j,:);
end

handles.accmax=24;

for i=1:handles.accmax
  handles.acc_all(i,:)=handles.chdata(8+i,:);
end

axes(handles.FSR_R_O);

for j=1:handles.nfsr
    plot(handles.timeall,fsr_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.FSR_L_O);

for j=5:8
    plot(handles.timeall,fsr_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.ACC_R_O);

for j=1:handles.accmax/2
    plot(handles.timeall,handles.acc_all(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

axes(handles.ACC_L_O);

for j=handles.accmax/2+1:handles.accmax
    plot(handles.timeall,handles.acc_all(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([handles.timeall(1) handles.timeall(length(handles.timeall))]);

[xmark,ymark]=ginput(2);


handles.tstarts=xmark(1);
handles.tstops=xmark(2);

if handles.tstarts > handles.tstops
    temps=handles.tstarts;
    handles.tstarts=handles.tstops;
    handles.tstops=temps;
elseif handles.tstarts == handles.tstops
    handles.tstops=handles.tstarts+0.1;
end

handles.istart=round(handles.tstarts/handles.tstep+1);
handles.istop=round(handles.tstops/handles.tstep+1);
if handles.istart < 1
    handles.istart=1;
end
if handles.istop > handles.nsamples
    handles.istop=handles.nsamples;
end
handles.tstarts=(handles.istart-1)*handles.tstep;
handles.tstops=(handles.istop-1)*handles.tstep;
handles.nwork=handles.istop-handles.istart+1;

handles.time=0:1:handles.nwork-1;
handles.time=handles.time*handles.tstep;

for i=1:8
    handles.fsr(i,:)=fsr_all(i,handles.istart:handles.istop);
end

handles.fsr(4,:)=handles.fsr(4,:)-50;
handles.fsrx=handles.fsr;

for i=1:handles.accmax
    handles.acc(i,:)=handles.acc_all(i,handles.istart:handles.istop);
end

handles.accx=handles.acc;

axes(handles.FSR_R_O);

for j=1:handles.nfsr
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.FSR_L_O);

for j=5:8
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_R_O);

for j=1:handles.accmax/2
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_L_O);

for j=(handles.accmax/2+1):handles.accmax
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);


set(handles.edit5,'String','');
set(handles.edit6,'String','');
set(handles.edit7,'String','');
set(handles.edit9,'String','');
set(handles.edit10,'String','');
set(handles.edit11,'String','');
set(handles.edit14,'String','');
set(handles.edit15,'String','');
set(handles.edit16,'String','');
set(handles.edit17,'String','');
set(handles.edit18,'String','');


guidata(hObject,handles)


% --- Executes on button press in Proc_signal_rb.
function Proc_signal_rb_Callback(hObject, eventdata, handles)
% hObject    handle to Proc_signal_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Proc_signal_rb

 if (get(handles.Proc_signal_rb,'Value') == get(handles.Proc_signal_rb,'Max'))
     set(handles.filter_pan,'Visible','off');
     set(handles.Opening_pan,'Visible','off');
     set(handles.Raw_pan,'Visible','off');
     set(handles.statistic_pan,'Visible','off');
     set(handles.processed_pan,'Visible','on');
     set(handles.Stat_rb,'Enable','on');
     set(handles.Statistic,'Enable','on');
     set(handles.Statistic_menu,'Enable','on');
     set(handles.uipushtool13,'Enable','off');
     set(handles.uipushtool14,'Enable','off');
     set(handles.Untitled_7,'Enable','off');
     set(handles.uitoggletool1,'Enable','on');
     set(handles.uitoggletool2,'Enable','on');
     set(handles.uitoggletool3,'Enable','on');
     cla(handles.R_GRF);
     cla(handles.L_GRF);
     set(handles.pushbutton4,'Visible','off');
     set(handles.pushbutton5,'Visible','off');
     
     
     tf=0;
     nfilhm=5;
     nfilm=10;
     bfilm=ones(1,nfilm)/nfilm;
     afilm=1;
     wn=handles.flpf/(0.5*handles.fsample);
     nfilh=round(1/wn)+1;  % Deo ptrebih koeficijenata za filtriranje, izmena sledi kad bude radjen RAW
     %nfilh=nfilh/2;
     [bfil,afil]=butter(1,wn);
     
     for i=1:handles.accmax/2
         accd(i,:)=handles.acc_all(2*i,:)-handles.acc_all(2*i-1,:);
     end
     
     
     heel=handles.fsr(4,:);      
     mtt=max(handles.fsr(1,:),handles.fsr(2,:));
     mtt=max(mtt,handles.fsr(3,:));

     heelL=handles.fsr(8,:);      
     mttL=max(handles.fsr(5,:),handles.fsr(6,:));
     mttL=max(mttL,handles.fsr(7,:));


     for i=1:handles.nwork
        if max(heel)>max(mtt), mtt(i)=mtt(i).*max(heel)/max(mtt);
        else if max(mtt)>max(heel),  heel(i)=heel(i).*max(mtt)/max(heel);  
             end
        end
     end

     for i=1:handles.nwork
     if max(heelL)>max(mttL), mttL(i)=mttL(i).*max(heelL)/max(mttL);
     else if max(mttL)>max(heelL),  heelL(i)=heelL(i).*max(mttL)/max(heelL);  
         end
     end
     end

    
     fsrall=heel+mtt;
     fsrall=fsrall/max(fsrall);
     fsrall=fsrall*(1.1/max(fsrall));

     fsrallL=heelL+mttL;
     fsrallL=fsrallL/max(fsrallL);
     fsrallL=fsrallL*(1.1/max(fsrallL));


     heel2=heel/max(heel);
     heel2L=heelL/max(heelL);
     mtt2=mtt/max(mtt);
     mtt2L=mttL/max(mttL);
  

     y_th=0.05;       
     for i=1:handles.nwork
        if fsrall(i)<y_th fsrall(i)=0;
        end
        if fsrallL(i)<y_th fsrallL(i)=0;
        end
        if heel2(i)<y_th heel2(i)=0;
        end
        if heel2L(i)<y_th heel2L(i)=0;
        end
        if mtt2(i)<y_th mtt2(i)=0;
        end
        if mtt2L(i)<y_th mtt2L(i)=0;
        end
    
     end

     fsrall4=fsrall.^(1/4);
     handles.GRF_v=handles.kk*fsrall4/max(fsrall4); %kk=koeficijent skaliranja 1.1 za zdrave, 0.9 za bolesne, definisan u case-ovima
 
     fsrallL=fsrallL.^(1/4);
     handles.GRF_vleft=handles.kk*fsrallL/max(fsrallL);


     handles.GRF_v=filter(bfilm,afilm,handles.GRF_v);
     handles.GRF_vleft=filter(bfilm,afilm,handles.GRF_vleft);
 

     GRF_hh=zeros(1);
     for i=1:handles.nwork
        if handles.GRF_v(i)>0.1 GRF_hh(i)=1;
        else GRF_hh(i)=0;
        end
     end

     hc_ind_left=[];
     to_ind_left=[];
     hc_val_left=[];
     to_val_left=[];


     for j=1:handles.nwork
        if handles.GRF_vleft(j)>0.1 GRF_vl(j)=1;
        else GRF_vl(j)=0;
        end
     end

     for i=2:handles.nwork
         if ((GRF_vl(i)==1) & (GRF_vl(i-1)==0))
              hc_val_left =[hc_val_left GRF_vl(i)];
              hc_ind_left = [ hc_ind_left i];
         end
         if ((GRF_vl(i)==0) & (GRF_vl(i-1)==1))
              to_val_left =[to_val_left GRF_vl(i-1)];
              to_ind_left = [to_ind_left i-1];
         end
    
     end


     hc_val=[];
     hc_ind=[];
     to_val=[];
     to_ind=[];

     for i=2:handles.nwork
         if ((GRF_hh(i)==1) & (GRF_hh(i-1)==0))
             hc_val =[hc_val GRF_hh(i)];
             hc_ind = [ hc_ind i];
         end
         if ((GRF_hh(i)==0) & (GRF_hh(i-1)==1))
             to_val =[to_val GRF_hh(i-1)];
             to_ind = [to_ind i-1];
         end
    
     end
     handles.rightdn=hc_ind*handles.tstep;
     handles.rightup=to_ind*handles.tstep;
     handles.neright=length(handles.rightdn)-1;
 
     handles.leftdn=hc_ind_left*handles.tstep;
     handles.leftup=to_ind_left*handles.tstep;
     handles.neleft=length(handles.leftdn)-1;
     
axes(handles.R_GRF);     
handles.GRF_h=mtt2-heel2;
handles.GRF_h=handles.GRF_h/5;
hold on
plot(handles.time,handles.GRF_v,'m');
plot(handles.time,handles.GRF_h);
hold off
legend('GRF Vertical','GRF Horizontal');
xlim([0 handles.tstops-handles.tstarts]);
box on;
 
axes(handles.L_GRF);
 
handles.GRF_hleft=mtt2L-heel2L;
handles.GRF_hleft=handles.GRF_hleft/5;
hold on
plot(handles.time,handles.GRF_vleft,'m');
plot(handles.time,handles.GRF_hleft);
hold off
legend('GRF Vertical','GRF Horizontal');
xlim([0 handles.tstops-handles.tstarts]);
box on;
%%%%%%%%%%%%%%%%%%%%%%%




nbutter_lp=1;
nbutter_hp=4;
handles.flpf=0.5;
handles.wn=handles.flpf/(0.5*handles.fsample);
[handles.bfilt,handles.afilt]=butter(nbutter_lp,handles.wn);
[handles.bfilt_hp,handles.afilt_hp]=butter(nbutter_hp,handles.wn,'high');


accdiff_thigh=handles.acc(2,:)-handles.acc(1,:); 
accdiff_shank=handles.acc(6,:)-handles.acc(5,:);
accdiff_foot=handles.acc(10,:)-handles.acc(9,:);


accdiff_knee=accdiff_thigh-accdiff_shank;
accdiff_ankle=accdiff_shank-accdiff_foot;

accdiff_knee=filtfilt(handles.bfilt,handles.afilt,accdiff_knee)*1.451319/handles.flpf^2/handles.length3;
accdiff_ankle=filtfilt(handles.bfilt,handles.afilt,accdiff_ankle)*1.451319/handles.flpf^2/handles.length3;

accdiff_knee_hp=filtfilt(handles.bfilt_hp,handles.afilt_hp,accdiff_knee);
accdiff_ankle_hp=filtfilt(handles.bfilt_hp,handles.afilt_hp,accdiff_ankle);

handles.knee=accdiff_knee_hp;
handles.ankle=accdiff_ankle_hp;
%%%%%%%%%%%%%%%%%%%
thigh_f=accdiff_thigh;
thigh_f=filtfilt(handles.bfilt,handles.afilt,thigh_f)*1.451319/handles.flpf^2/handles.length3;
thigh_f=filtfilt(handles.bfilt_hp,handles.afilt_hp,thigh_f);
handles.hip=0-2*thigh_f;
handles.hip=handles.hip-mean(handles.hip);


% Lthigh_f=filtfilt(bfil,afil,accd(7,:))*1.451319/handles.flpf^2;
% Lshank_f=filtfilt(bfil,afil,accd(9,:))*1.451319/handles.flpf^2;
% Lfoot_f=filtfilt(bfil,afil,accd(11,:))*1.451319/handles.flpf^2;
% Lhip=0-2*Lthigh_f;
% Lhip=Lhip-mean(Lhip);
% 
% Lknee=Lthigh_f-Lshank_f;
% Lknee=Lknee-min(Lknee);
% 
% Lankle=Lshank_f-Lfoot_f;
% Lankle=Lankle-mean(Lankle);

axes(handles.R_hip);

plot(handles.time,handles.hip,'m','Linewidth',3);
xlim([0 handles.tstops-handles.tstarts]);
grid;

axes(handles.L_hip);

plot(handles.time,handles.hip,'b','Linewidth',3); % FAKE
xlim([0 handles.tstops-handles.tstarts]);
grid;

axes(handles.R_knee);

plot(handles.time,handles.knee,'m','Linewidth',3);
xlim([0 handles.tstops-handles.tstarts]);
grid;

axes(handles.L_knee);

plot(handles.time,handles.gon_all(1,handles.istart:handles.istop),'b','Linewidth',3);
xlim([0 handles.tstops-handles.tstarts]);
grid;

axes(handles.R_ankle);

plot(handles.time,handles.ankle,'m','Linewidth',3);
xlim([0 handles.tstops-handles.tstarts]);
grid;

axes(handles.L_ankle);
plot(handles.time,handles.gon_all(2,handles.istart:handles.istop),'b','Linewidth',3);
xlim([0 handles.tstops-handles.tstarts]);
grid;

 else 
     
     set(handles.Proc_signal_rb,'Value',1);
     
 end
  guidata(hObject,handles);   
     

% --- Executes on button press in Stat_rb.
function Stat_rb_Callback(hObject, eventdata, handles)
% hObject    handle to Stat_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Stat_rb

if (get(handles.Stat_rb,'Value') == get(handles.Stat_rb,'Max'))
     
     handles.namecoef=0;
     handles.diagnosiscoef=0;
     set(handles.Opening_pan,'Visible','off');
     set(handles.Raw_pan,'Visible','off');
     set(handles.processed_pan,'Visible','off');
     set(handles.filter_pan,'Visible','off');
     set(handles.statistic_pan,'Visible','on');
     set(handles.uipushtool13,'Enable','on');
     set(handles.uipushtool14,'Enable','off');
     set(handles.Untitled_7,'Enable','on');
     set(handles.text72,'Visible','off')
     set(handles.edit5,'String','');
     set(handles.edit6,'String','');
     set(handles.edit7,'String','');
     set(handles.edit9,'String','');
     set(handles.edit10,'String','');
     set(handles.edit11,'String','');
     set(handles.text73,'Visible','off')
     set(handles.edit13,'String','');
     set(handles.edit14,'String','');
     set(handles.edit15,'String','');
     set(handles.edit16,'String','');
     set(handles.edit17,'String','');
     set(handles.edit18,'String','');
     set(handles.uitoggletool1,'Enable','off');
     set(handles.uitoggletool2,'Enable','off');
     set(handles.uitoggletool3,'Enable','off');
     set(handles.pushbutton4,'Visible','on');
     set(handles.pushbutton5,'Visible','on');
     
     [handles.Step_Count,handles.Ambulation_Time,handles.Cadence,handles.Step_Time_Differential_Av,handles.Cycle_Time_Differential_Av,handles.Step_Time_Right_Av,handles.Step_Time_Left_Av,handles.Cycle_Time_Right_Av,handles.Cycle_Time_Left_Av,handles.Swing_Time_Right_Av,handles.Swing_Time_Left_Av,handles.Stance_Time_Right_Av,handles.Stance_Time_Left_Av,handles.Single_Supp_Time_Right_Av,handles.Single_Supp_Time_Left_Av,handles.Double_Support_Time_Right_Av,handles.Double_Support_Time_Left_Av,handles.Swing_Cycle_Right_Av,handles.Swing_Cycle_Left_Av,handles.Stance_Cycle_Right_Av,handles.Stance_Cycle_Left_Av,handles.Single_Supp_Cycle_Right_Av,handles.Single_Supp_Cycle_Left_Av,handles.Double_Support_Cycle_Right_Av,handles.Double_Support_Cycle_Left_Av,handles.Heel_Off_On_Time_Right_Av,handles.Heel_Off_On_Time_Left_Av,handles.Step_Time_Right_Std,handles.Step_Time_Left_Std,handles.Stride_Time_Right_Std,handles.Stride_Time_Left_Std,handles.Swing_Time_Right_Std,handles.Swing_Time_Left_Std,handles.Stance_Time_Right_Std,handles.Stance_Time_Left_Std,handles.Single_Support_Right_Std,handles.Single_Support_Left_Std,handles.Double_Support_Time_Right_Std,handles.Double_Support_Time_Left_Std,handles.Heel_Off_On_Right_Std,handles.Heel_Off_On_Left_Std,handles.Heel_Off_On_Cycle_Av_L,handles.Heel_Off_On_Cycle_Av_R] = statistika(handles.chdata,handles.istart,handles.istop,handles.nsamples,handles.tstarts,handles.tstops);

     if handles.Step_Count>0
         
set(handles.name,'Enable','on');
set(handles.excel,'Enable','on');
set(handles.diagnosis,'Enable','on');
set(handles.pushbutton4,'Enable','on');
set(handles.edit5,'String',handles.Stance_Time_Right_Av);
set(handles.edit9,'String',handles.Stance_Time_Right_Std);    
set(handles.edit7,'String',handles.Swing_Time_Right_Av);
set(handles.edit11,'String',handles.Swing_Time_Right_Std);
set(handles.edit6,'String',handles.Cycle_Time_Right_Av);
set(handles.edit10,'String',handles.Stride_Time_Right_Std);
set(handles.edit13,'String',handles.Stance_Time_Left_Av);
set(handles.edit16,'String',handles.Stance_Time_Left_Std);
set(handles.edit15,'String',handles.Swing_Time_Left_Av);
set(handles.edit18,'String',handles.Swing_Time_Left_Std);
set(handles.edit14,'String',handles.Cycle_Time_Left_Av);
set(handles.edit17,'String',handles.Stride_Time_Left_Std);
set(handles.edit61,'String',handles.Step_Count);
set(handles.edit62,'String',handles.Ambulation_Time);
set(handles.edit63,'String',handles.Cadence);
set(handles.edit64,'String',handles.Step_Time_Differential_Av);
set(handles.edit65,'String',handles.Cycle_Time_Differential_Av);
set(handles.edit21,'String',handles.Step_Time_Right_Av);
set(handles.edit22,'String',handles.Single_Supp_Time_Right_Av);
set(handles.edit23,'String',handles.Double_Support_Time_Right_Av);
set(handles.edit24,'String',handles.Step_Time_Right_Std);
set(handles.edit25,'String',handles.Single_Support_Right_Std);
set(handles.edit26,'String',handles.Double_Support_Time_Right_Std);
set(handles.edit27,'String',handles.Step_Time_Left_Av);
set(handles.edit28,'String',handles.Single_Supp_Time_Left_Av);
set(handles.edit29,'String',handles.Double_Support_Time_Left_Av);
set(handles.edit30,'String',handles.Step_Time_Left_Std);
set(handles.edit31,'String',handles.Single_Support_Left_Std);
set(handles.edit32,'String',handles.Double_Support_Time_Left_Std);
set(handles.edit33,'String',handles.Swing_Cycle_Right_Av);
set(handles.edit34,'String',handles.Stance_Cycle_Right_Av);
set(handles.edit35,'String',handles.Single_Supp_Cycle_Right_Av);
set(handles.edit39,'String',handles.Swing_Cycle_Left_Av);
set(handles.edit40,'String',handles.Stance_Cycle_Left_Av);
set(handles.edit41,'String',handles.Single_Supp_Cycle_Left_Av);
set(handles.edit45,'String',handles.Double_Support_Cycle_Right_Av);
set(handles.edit57,'String',handles.Heel_Off_On_Time_Right_Av);
set(handles.edit47,'String',handles.Heel_Off_On_Cycle_Av_R);
set(handles.edit58,'String',handles.Heel_Off_On_Right_Std);
set(handles.edit51,'String',handles.Double_Support_Cycle_Left_Av);
set(handles.edit59,'String',handles.Heel_Off_On_Time_Left_Av);
set(handles.edit53,'String',handles.Heel_Off_On_Cycle_Av_L);
set(handles.edit60,'String',handles.Heel_Off_On_Left_Std);

     else

set(handles.Print,'Enable','off');
set(handles.excel,'Enable','off');
set(handles.name,'Enable','off');
set(handles.diagnosis,'Enable','off');
set(handles.pushbutton4,'Enable','off');      
set(handles.text72,'Visible','on');
set(handles.text73,'Visible','on');
set(handles.edit61,'String','Not available');
set(handles.edit62,'String','Not available');
set(handles.edit63,'String','Not available');
set(handles.edit64,'String','Not available');
set(handles.edit65,'String','Not available');
set(handles.edit21,'String','Not available');
set(handles.edit22,'String','Not available');
set(handles.edit23,'String','Not available');
set(handles.edit24,'String','Not available');
set(handles.edit25,'String','Not available');
set(handles.edit26,'String','Not available');
set(handles.edit27,'String','Not available');
set(handles.edit28,'String','Not available');
set(handles.edit29,'String','Not available');
set(handles.edit30,'String','Not available');
set(handles.edit31,'String','Not available');
set(handles.edit32,'String','Not available');
set(handles.edit33,'String','Not available');
set(handles.edit34,'String','Not available');
set(handles.edit35,'String','Not available');
set(handles.edit39,'String','Not available');
set(handles.edit40,'String','Not available');
set(handles.edit41,'String','Not available');
set(handles.edit45,'String','Not available');
set(handles.edit57,'String','Not available');
set(handles.edit47,'String','Not available');
set(handles.edit58,'String','Not available');
set(handles.edit51,'String','Not available');
set(handles.edit59,'String','Not available');
set(handles.edit53,'String','Not available');
set(handles.edit60,'String','Not available');
set(handles.edit5,'String','Not available')
set(handles.edit6,'String','Not available');
set(handles.edit7,'String','Not available');
set(handles.edit9,'String','Not available');
set(handles.edit10,'String','Not available');
set(handles.edit11,'String','Not available');
set(handles.edit13,'String','Not available');
set(handles.edit14,'String','Not available');
set(handles.edit15,'String','Not available');
set(handles.edit16,'String','Not available');
set(handles.edit17,'String','Not available');
set(handles.edit18,'String','Not available');

     end

 
else
    
    set(handles.Stat_rb,'Value',1);
     
end

guidata(hObject,handles);

function name_Callback(hObject, eventdata, handles)
% hObject    handle to name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of name as text
%        str2double(get(hObject,'String')) returns contents of name as a double


handles.namex=get(hObject,'String');
bla='';

if strcmp(handles.namex,bla)==1

   handles.namecoef=0;

else
    
    handles.namecoef=1;

end

guidata(hObject,handles);

% --- Executes during object creation, after setting all properties.
function name_CreateFcn(hObject, eventdata, handles)
% hObject    handle to name (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function diagnosis_Callback(hObject, eventdata, handles)
% hObject    handle to diagnosis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of diagnosis as text
%        str2double(get(hObject,'String')) returns contents of diagnosis as a double

handles.diagnosisx=get(hObject,'String');
bla='';

if strcmp(handles.diagnosisx,bla)==1

   handles.diagnosiscoef=0;

else
    
    handles.diagnosiscoef=1;

end

guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function diagnosis_CreateFcn(hObject, eventdata, handles)
% hObject    handle to diagnosis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit9_Callback(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit9 as text
%        str2double(get(hObject,'String')) returns contents of edit9 as a double


% --- Executes during object creation, after setting all properties.
function edit9_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit12_Callback(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit12 as text
%        str2double(get(hObject,'String')) returns contents of edit12 as a double


% --- Executes during object creation, after setting all properties.
function edit12_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit13_Callback(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit13 as text
%        str2double(get(hObject,'String')) returns contents of edit13 as a double


% --- Executes during object creation, after setting all properties.
function edit13_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit14_Callback(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit14 as text
%        str2double(get(hObject,'String')) returns contents of edit14 as a double


% --- Executes during object creation, after setting all properties.
function edit14_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit15_Callback(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit15 as text
%        str2double(get(hObject,'String')) returns contents of edit15 as a double


% --- Executes during object creation, after setting all properties.
function edit15_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit16_Callback(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit16 as text
%        str2double(get(hObject,'String')) returns contents of edit16 as a double


% --- Executes during object creation, after setting all properties.
function edit16_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit17_Callback(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit17 as text
%        str2double(get(hObject,'String')) returns contents of edit17 as a double


% --- Executes during object creation, after setting all properties.
function edit17_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit18_Callback(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit18 as text
%        str2double(get(hObject,'String')) returns contents of edit18 as a double


% --- Executes during object creation, after setting all properties.
function edit18_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filew=strrep(handles.file,'.gaz','_parameters.csv');
fidw=fopen(filew,'w');

if handles.namecoef==1 && handles.diagnosiscoef==0
    
    fprintf(fidw,'Patients name: %s\n',handles.namex);
    
elseif handles.namecoef==0 && handles.diagnosiscoef==1
    
    fprintf(fidw,'Diagnosis: %s\n',handles.diagnosisx);
    
elseif handles.namecoef==1 && handles.diagnosiscoef==1
    
    fprintf(fidw,'Patients name: %s\n',handles.namex);    
    fprintf(fidw,'Diagnosis: %s\n',handles.diagnosisx);
    
end

fprintf(fidw,'\n');
fprintf(fidw,'\n');
    
fprintf(fidw,'Parameter, Left/Total, Right\n');
fprintf(fidw,'\n');
fprintf(fidw,'Step Count, %i\n',handles.Step_Count);
fprintf(fidw,'Ambulation Time [s], %8.3f\n',handles.Ambulation_Time);
fprintf(fidw,'Cadence [steps/min], %8.3f\n',handles.Cadence);
fprintf(fidw,'Step Time Differential Av. [s], %8.3f\n',handles.Step_Time_Differential_Av);
fprintf(fidw,'Cycle Time Differential Av. [s], %8.3f\n',handles.Cycle_Time_Differential_Av);
fprintf(fidw,'Step Time Av. [s], %8.3f, %8.3f\n',handles.Step_Time_Left_Av,handles.Step_Time_Right_Av);
fprintf(fidw,'Cycle (Stride) Time Av. [s], %8.3f, %8.3f\n',handles.Cycle_Time_Left_Av,handles.Cycle_Time_Right_Av);
fprintf(fidw,'Swing Time Av. [s], %8.3f, %8.3f\n',handles.Swing_Time_Left_Av,handles.Swing_Time_Right_Av);
fprintf(fidw,'Stance Time Av. [s], %8.3f, %8.3f\n',handles.Stance_Time_Left_Av,handles.Stance_Time_Right_Av);
fprintf(fidw,'Single Supp. Time Av. [s], %8.3f, %8.3f\n',handles.Swing_Time_Right_Av,handles.Swing_Time_Left_Av);
fprintf(fidw,'Double Support Time Av. [s], %8.3f, %8.3f\n',handles.Double_Support_Time_Left_Av,handles.Double_Support_Time_Right_Av);
fprintf(fidw,'Swing %% Cycle Av., %8.3f, %8.3f\n',handles.Swing_Time_Left_Av*100/handles.Cycle_Time_Left_Av,handles.Swing_Time_Right_Av*100/handles.Cycle_Time_Right_Av);
fprintf(fidw,'Stance %% Cycle Av., %8.3f, %8.3f\n',handles.Stance_Time_Left_Av*100/handles.Cycle_Time_Left_Av,handles.Stance_Time_Right_Av*100/handles.Cycle_Time_Right_Av);
fprintf(fidw,'Single Supp. %% Cycle Av., %8.3f, %8.3f\n',handles.Swing_Time_Left_Av*100/handles.Cycle_Time_Left_Av,handles.Swing_Time_Right_Av*100/handles.Cycle_Time_Right_Av);
fprintf(fidw,'Double Support %% Cycle Av., %8.3f, %8.3f\n',handles.Double_Support_Time_Left_Av*100/handles.Cycle_Time_Left_Av,handles.Double_Support_Time_Right_Av*100/handles.Cycle_Time_Right_Av);
fprintf(fidw,'Heel Off On Time Av. [s], %8.3f, %8.3f\n',handles.Heel_Off_On_Time_Left_Av,handles.Heel_Off_On_Time_Right_Av);
fprintf(fidw,'Heel Off On %% Cycle Av. [s], %8.3f, %8.3f\n',handles.Heel_Off_On_Time_Left_Av*100/handles.Cycle_Time_Left_Av,handles.Heel_Off_On_Time_Right_Av*100/handles.Cycle_Time_Right_Av);
fprintf(fidw,'Step Time Std. [s], %8.3f, %8.3f\n',handles.Step_Time_Left_Std,handles.Step_Time_Right_Std);
fprintf(fidw,'Stride Time Std. [s], %8.3f, %8.3f\n',handles.Stride_Time_Left_Std,handles.Stride_Time_Right_Std);
fprintf(fidw,'Swing Time Std. [s], %8.3f, %8.3f\n',handles.Swing_Time_Left_Std,handles.Swing_Time_Right_Std);
fprintf(fidw,'Stance Time Std. [s], %8.3f, %8.3f\n',handles.Stance_Time_Left_Std,handles.Stance_Time_Right_Std);
fprintf(fidw,'Single Support Std. [s], %8.3f, %8.3f\n',handles.Swing_Time_Right_Std,handles.Swing_Time_Left_Std);
fprintf(fidw,'Double Support Time Std. [s], %8.3f, %8.3f\n',handles.Double_Support_Time_Left_Std,handles.Double_Support_Time_Right_Std);
fprintf(fidw,'Heel Off On Std. [s], %8.3f, %8.3f\n',handles.Heel_Off_On_Left_Std,handles.Heel_Off_On_Right_Std);

fprintf(fidw,'\n');
fprintf(fidw,'\n');


fclose(fidw);


filew1=strrep(handles.file,'.gaz','_data.csv');
if strcmp(filew,handles.file)
    filew1='fsr_acc2.csv';
end
fidw1=fopen(filew1,'w');

fprintf(fidw1,' samples,    fsr1,     fsr2,     fsr3,     fsr4,     fsr5,     fsr6,     fsr7,    fsr8,    GRF V,     GRF H,     hip,     knee,     ankle \n');
fprintf(fidw1,'\n');

for i=1:handles.nwork
    fprintf(fidw1,'%8.f, ',i);
    
    for j=1:8
        fprintf(fidw,'%8.2f, ',handles.fsr(j,i));
    end
    
        fprintf(fidw,'%8.3f, ',handles.GRF_v(i));
        fprintf(fidw,'%8.3f, ',handles.GRF_h(i));
        fprintf(fidw,'%8.3f, ',handles.hip(i));
        fprintf(fidw,'%8.3f, ',handles.knee(i));
        fprintf(fidw,'%8.3f, ',handles.ankle(i));
   
    fprintf(fidw1,'\n');
end

fclose(fidw1);

guidata(hObject,handles);


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

filew=strrep(handles.file,'.gaz','_angles.txt');
if strcmp(filew,handles.file)
    filew='fsr_acc2.txt';
end
fidw=fopen(filew,'w');

fprintf(fidw,' samples    fsr1     fsr2     fsr3     fsr4     fsr5     fsr6     fsr7    fsr8    GRF V     GRF H     hip     knee     ankle \n');
fprintf(fidw,'\n');

for i=1:handles.nwork
    fprintf(fidw,'%8.f ',i);
    
    for j=1:8
        fprintf(fidw,'%8.2f ',handles.fsr(j,i));
    end
    
        fprintf(fidw,'%8.3f ',handles.GRF_v(i));
        fprintf(fidw,'%8.3f ',handles.GRF_h(i));
        fprintf(fidw,'%8.3f ',handles.hip(i));
        fprintf(fidw,'%8.3f ',handles.knee(i));
        fprintf(fidw,'%8.3f ',handles.ankle(i));
   
    fprintf(fidw,'\n');
end

fclose(fidw);

guidata(hObject,handles);

% --- Executes on button press in Raw_signal_rb.
function Raw_signal_rb_Callback(hObject, eventdata, handles)
% hObject    handle to Raw_signal_rb (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of Raw_signal_rb

 if (get(handles.Raw_signal_rb,'Value') == get(handles.Raw_signal_rb,'Max'))
     
     set(handles.pushbutton4,'Visible','off');
     set(handles.pushbutton5,'Visible','off');
     set(handles.Opening_pan,'Visible','off');
     set(handles.statistic_pan,'Visible','off');
     set(handles.processed_pan,'Visible','off');
     set(handles.Raw_pan,'Visible','on');
     set(handles.filter_pan,'Visible','on');
     set(handles.Stat_rb,'Enable','off');
     set(handles.Statistic,'Enable','off');
     set(handles.Statistic_menu,'Enable','off');
     set(handles.uipushtool13,'Enable','off');
     set(handles.uipushtool14,'Enable','off');
     set(handles.Untitled_7,'Enable','off');
     handles.niz1=ones(1,4);
     handles.niz2=ones(1,4);
     handles.niz3=ones(1,12);
     handles.niz4=ones(1,12);
     set(handles.popupmenu1,'Value',1);
     set(handles.edit20,'Enable','off');
     set(handles.edit19,'Enable','off');
     set(handles.text51,'Visible','on');
     set(handles.text52,'Visible','off');
     set(handles.text53,'Visible','on');
     set(handles.text54,'Visible','off');          
     set(handles.pushbutton7,'Enable','off');
     set(handles.edit19,'String','')
     set(handles.edit20,'String','');
     set(handles.uitoggletool1,'Enable','on');
     set(handles.uitoggletool2,'Enable','on');
     set(handles.uitoggletool3,'Enable','on');
     
    set(handles.checkbox46,'Value',1);
    set(handles.checkbox49,'Value',1);
    set(handles.checkbox48,'Value',1);
    set(handles.checkbox47,'Value',1);
    set(handles.checkbox5,'Value',1);
    set(handles.checkbox8,'Value',1);
    set(handles.checkbox7,'Value',1);
    set(handles.checkbox6,'Value',1);
    set(handles.checkbox22,'Value',1);
    set(handles.checkbox23,'Value',1);
    set(handles.checkbox24,'Value',1);
    set(handles.checkbox25,'Value',1);
    set(handles.checkbox26,'Value',1);
    set(handles.checkbox27,'Value',1);
    set(handles.checkbox28,'Value',1);
    set(handles.checkbox29,'Value',1);
    set(handles.checkbox30,'Value',1);
    set(handles.checkbox31,'Value',1);
    set(handles.checkbox32,'Value',1);
    set(handles.checkbox33,'Value',1);
    set(handles.checkbox34,'Value',1);
    set(handles.checkbox35,'Value',1);
    set(handles.checkbox36,'Value',1);
    set(handles.checkbox37,'Value',1);
    set(handles.checkbox38,'Value',1);
    set(handles.checkbox39,'Value',1);
    set(handles.checkbox40,'Value',1);
    set(handles.checkbox41,'Value',1);
    set(handles.checkbox42,'Value',1);
    set(handles.checkbox43,'Value',1);
    set(handles.checkbox44,'Value',1);
    set(handles.checkbox45,'Value',1);
     
     
     
axes(handles.FSR_R_R);

for j=1:handles.nfsr
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.FSR_L_R);

for j=5:8
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_R_R);

for j=1:handles.accmax/2
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

axes(handles.ACC_L_R);

for j=(handles.accmax/2+1):handles.accmax
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

 else
     
     set(handles.Raw_signal_rb,'Value',1);
       
 end
 
 

 guidata(hObject,handles)
 

% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = get(hObject,'String') returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1

   val = get(hObject,'Value');
 
      switch val
          
          case 1
          
 
              handles.filter_name='None';
              set(handles.edit20,'Enable','off');
              set(handles.edit19,'Enable','off');
              set(handles.text51,'Visible','on');
              set(handles.text52,'Visible','off');
              set(handles.text53,'Visible','on');
              set(handles.text54,'Visible','off');          
              set(handles.pushbutton7,'Enable','off');
              set(handles.edit19,'String','');
              set(handles.edit20,'String','');
              handles.fsrx=handles.fsr;
              handles.accx=handles.acc;
              
              
%%%%%%%%%%%%%  deo za fsrr  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        
        
           for i=1:4
            
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
    
                    if handles.niz1(1,i)==1
        
                    axes(handles.FSR_R_R);

                        plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
                        hold on;
                    end

                    xlim([0 handles.tstops-handles.tstarts]);

           end
           hold off;


                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');

%%%%%%%%%%  deo za fsrl   %%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    

            for i=1:4
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
                    if handles.niz2(1,i)==1

                       axes(handles.FSR_L_R);

                        plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
                        hold on;
                    end

                    xlim([0 handles.tstops-handles.tstarts]);

            end
            hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');
    
%%%%%%%%%%%%%%%%% deo za accr  %%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
                    if handles.niz3(1,i)==1

                       axes(handles.ACC_R_R);

                       plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
                       hold on;
                    end

                       xlim([0 handles.tstops-handles.tstarts]);

            end
            hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');


%%%%%%%%%%%%%%%  deo za accl   %%%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
            if handles.niz4(1,i)==1
        
                axes(handles.ACC_L_R);

                plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
                hold on;
            end

                xlim([0 handles.tstops-handles.tstarts]);

            end
            hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');
              
             
          
          case 2
              
              
              handles.freqency=5;
              handles.nfil=324;
              handles.filter_name='FIR';
              set(handles.edit20,'Enable','on');
              set(handles.edit19,'Enable','on');
              set(handles.text51,'Visible','on');
              set(handles.text52,'Visible','off');
              set(handles.text53,'Visible','on');
              set(handles.text54,'Visible','off');          
              set(handles.pushbutton7,'Enable','on');
              set(handles.edit19,'String',handles.freqency)
              set(handles.edit20,'String',handles.nfil);
              
              
          case 3
              
              handles.freqency=5;
              handles.nfil=1;
              handles.filter_name='Butter';
              set(handles.edit20,'Enable','on');
              set(handles.edit19,'Enable','on');
              set(handles.text51,'Visible','on');
              set(handles.text52,'Visible','off');
              set(handles.text53,'Visible','on');
              set(handles.text54,'Visible','off');          
              set(handles.pushbutton7,'Enable','on');
              set(handles.edit19,'String',handles.freqency)
              set(handles.edit20,'String',handles.nfil);
              
          case 4
              
              handles.frequency=20;
              handles.filter_name='Average';
              set(handles.edit20,'Enable','off');
              set(handles.edit19,'Enable','on');
              set(handles.text51,'Visible','off');
              set(handles.text52,'Visible','on');
              set(handles.text53,'Visible','off');
              set(handles.text54,'Visible','on');          
              set(handles.pushbutton7,'Enable','on');
              set(handles.edit19,'String',handles.frequency);
              set(handles.edit20,'String','');
            
              
              
              
              
      end
              
         guidata(hObject,handles); 

% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit19_Callback(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit19 as text
%        str2double(get(hObject,'String')) returns contents of edit19 as a double


% --- Executes during object creation, after setting all properties.
function edit19_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton7.
function pushbutton7_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

switch handles.filter_name
    
    case 'FIR'
        
        handles.nfil=str2num(get(handles.edit20,'String'));
                
        if mod(handles.nfil,2)==1
            
            errordlg('You must enter even number for FIR filter order value ','FIR filter order value error');
            set(handles.edit20,'String','');
            
        else
        
        
        handles.frequency=str2num(get(handles.edit19,'String'));

        handles.wn=handles.frequency/(0.5*handles.fsample);
               
        handles.bfil=fir1(handles.nfil,handles.wn);
        handles.afil=1;
        
        handles.tf=handles.nfil/2;
        
        for i=1:handles.accmax
        handles.afp(i,:)=filter(handles.bfil,handles.afil,handles.acc_all(i,:))
        end
        
        for i=1:handles.accmax
        handles.accx(i,:)=handles.afp(i,((handles.istart+handles.tf):(handles.istop+handles.tf)))
        end
        
 
        
             

        
        
%%%%%%%%%%%%%%%%% deo za accr  %%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
                    if handles.niz3(1,i)==1

                       axes(handles.ACC_R_R);

                       plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
                       hold on;
                    end

                  xlim([0 handles.tstops-handles.tstarts]);

            end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');


%%%%%%%%%%%%%%%  deo za accl   %%%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
            if handles.niz4(1,i)==1
        
                axes(handles.ACC_L_R);

                plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
                hold on;
            end

                 xlim([0 handles.tstops-handles.tstarts]);

        end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');
        end             
                
    case 'Butter'
        
        handles.frequency=str2num(get(handles.edit19,'String'));
        handles.nfil=round((str2num(get(handles.edit20,'String')))/2);
        
        handles.wn=handles.frequency/(0.5*handles.fsample);
        
        [handles.bfil,handles.afil]=butter(handles.nfil,handles.wn);
        
        for i=1:handles.accmax
        accf(i,:)=filtfilt(handles.bfil,handles.afil,handles.acc(i,:));
        end
        
        handles.accx=accf;
        
        for i=1:8
        fsrf(i,:)=filtfilt(handles.bfil,handles.afil,handles.fsr(i,:));
        end
        
        handles.fsrx=fsrf;
        
    
%%%%%%%%%%%%%%%%% deo za accr  %%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
                    if handles.niz3(1,i)==1

                       axes(handles.ACC_R_R);

                       plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
                       hold on;
                    end

                       xlim([0 handles.tstops-handles.tstarts]);

            end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');


%%%%%%%%%%%%%%%  deo za accl   %%%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
            if handles.niz4(1,i)==1
        
                axes(handles.ACC_L_R);

                plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
                hold on;
            end

                xlim([0 handles.tstops-handles.tstarts]);

        end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');
                
                
    case 'Average'
        
        handles.frequency=str2num(get(handles.edit19,'String'));
        handles.bfil=ones(1,handles.frequency)/handles.frequency;
        handles.afil=1;
        
        handles.tf=round(handles.frequency/2);
        
        for i=1:handles.accmax
        afp1(i,:)=filter(handles.bfil,handles.afil,handles.acc_all(i,:));
        end
        
%         handles.accx=afp1;
        
        for i=1:handles.accmax
        handles.accx(i,:)=afp1(i,((handles.istart+handles.tf):(handles.istop+handles.tf)));
        end

    
%%%%%%%%%%%%%%%%% deo za accr  %%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
                    if handles.niz3(1,i)==1

                       axes(handles.ACC_R_R);

                       plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
                       hold on;
                    end

                       xlim([0 handles.tstops-handles.tstarts]);

            end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');


%%%%%%%%%%%%%%%  deo za accl   %%%%%%%%%%%%%%%%%%%%%%%

            for i=1:12
    
                set(handles.checkbox46,'Enable','off');
                set(handles.checkbox49,'Enable','off');
                set(handles.checkbox48,'Enable','off');
                set(handles.checkbox47,'Enable','off');
                set(handles.checkbox5,'Enable','off');
                set(handles.checkbox8,'Enable','off');
                set(handles.checkbox7,'Enable','off');
                set(handles.checkbox6,'Enable','off');
                set(handles.checkbox22,'Enable','off');
                set(handles.checkbox23,'Enable','off');
                set(handles.checkbox24,'Enable','off');
                set(handles.checkbox25,'Enable','off');
                set(handles.checkbox26,'Enable','off');
                set(handles.checkbox27,'Enable','off');
                set(handles.checkbox28,'Enable','off');
                set(handles.checkbox29,'Enable','off');
                set(handles.checkbox30,'Enable','off');
                set(handles.checkbox31,'Enable','off');
                set(handles.checkbox32,'Enable','off');
                set(handles.checkbox33,'Enable','off');
                set(handles.checkbox34,'Enable','off');
                set(handles.checkbox35,'Enable','off');
                set(handles.checkbox36,'Enable','off');
                set(handles.checkbox37,'Enable','off');
                set(handles.checkbox38,'Enable','off');
                set(handles.checkbox39,'Enable','off');
                set(handles.checkbox40,'Enable','off');
                set(handles.checkbox41,'Enable','off');
                set(handles.checkbox42,'Enable','off');
                set(handles.checkbox43,'Enable','off');
                set(handles.checkbox44,'Enable','off');
                set(handles.checkbox45,'Enable','off');
 
            if handles.niz4(1,i)==1
        
                axes(handles.ACC_L_R);

                plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
                hold on;
            end

                xlim([0 handles.tstops-handles.tstarts]);

        end
    hold off;

                set(handles.checkbox46,'Enable','on');
                set(handles.checkbox49,'Enable','on');
                set(handles.checkbox48,'Enable','on');
                set(handles.checkbox47,'Enable','on');
                set(handles.checkbox5,'Enable','on');
                set(handles.checkbox8,'Enable','on');
                set(handles.checkbox7,'Enable','on');
                set(handles.checkbox6,'Enable','on');
                set(handles.checkbox22,'Enable','on');
                set(handles.checkbox23,'Enable','on');
                set(handles.checkbox24,'Enable','on');
                set(handles.checkbox25,'Enable','on');
                set(handles.checkbox26,'Enable','on');
                set(handles.checkbox27,'Enable','on');
                set(handles.checkbox28,'Enable','on');
                set(handles.checkbox29,'Enable','on');
                set(handles.checkbox30,'Enable','on');
                set(handles.checkbox31,'Enable','on');
                set(handles.checkbox32,'Enable','on');
                set(handles.checkbox33,'Enable','on');
                set(handles.checkbox34,'Enable','on');
                set(handles.checkbox35,'Enable','on');
                set(handles.checkbox36,'Enable','on');
                set(handles.checkbox37,'Enable','on');
                set(handles.checkbox38,'Enable','on');
                set(handles.checkbox39,'Enable','on');
                set(handles.checkbox40,'Enable','on');
                set(handles.checkbox41,'Enable','on');
                set(handles.checkbox42,'Enable','on');
                set(handles.checkbox43,'Enable','on');
                set(handles.checkbox44,'Enable','on');
                set(handles.checkbox45,'Enable','on');

        
             
        
                

 
end

guidata(hObject,handles);
        

% --- Executes on button press in checkbox1.
function checkbox1_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox1


% --- Executes on button press in checkbox2.
function checkbox2_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox2


% --- Executes on button press in checkbox3.
function checkbox3_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox3


% --- Executes on button press in checkbox4.
function checkbox4_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox4


% --- Executes on button press in checkbox5.
function checkbox5_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox5

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz2(1)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    

else

    handles.niz2(1)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
    
    elseif sum(handles.niz2)==0 

    cla(handles.FSR_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);




% --- Executes on button press in checkbox6.
function checkbox6_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox6


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz2(4)=1;

for i=1:4
    
        set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz2(4)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
    
    elseif sum(handles.niz2)==0 

    cla(handles.FSR_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;   

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox7.
function checkbox7_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox7

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz2(3)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    

else

    handles.niz2(3)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
    
    elseif sum(handles.niz2)==0 

    cla(handles.FSR_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox8.
function checkbox8_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox8


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz2(2)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    

else

    handles.niz2(2)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz2(1,i)==1
        
axes(handles.FSR_L_R);

    plot(handles.time,handles.fsrx(i+4,:),'Color',handles.color3(i+4,:));
    hold on;
    
    elseif sum(handles.niz2)==0 

    cla(handles.FSR_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox9.
function checkbox9_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox9


% --- Executes on button press in checkbox10.
function checkbox10_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox10


% --- Executes on button press in checkbox11.
function checkbox11_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox11


% --- Executes on button press in checkbox12.
function checkbox12_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox12


% --- Executes on button press in checkbox14.
function checkbox14_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox14


% --- Executes on button press in checkbox15.
function checkbox15_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox15


% --- Executes on button press in checkbox16.
function checkbox16_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox16


% --- Executes on button press in checkbox17.
function checkbox17_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox17 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox17


% --- Executes on button press in checkbox18.
function checkbox18_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox18 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox18


% --- Executes on button press in checkbox19.
function checkbox19_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox19 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox19


% --- Executes on button press in checkbox20.
function checkbox20_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox20


% --- Executes on button press in checkbox21.
function checkbox21_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox21


% --- Executes on button press in checkbox22.
function checkbox22_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox22


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(1)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(1)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox23.
function checkbox23_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox23


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(4)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(4)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox24.
function checkbox24_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox24

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(3)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    

else

    handles.niz4(3)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox25.
function checkbox25_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox25

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(2)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(2)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;   

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox26.
function checkbox26_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox26


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(5)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(5)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox27.
function checkbox27_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox27


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(8)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(8)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox28.
function checkbox28_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox28


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(7)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(7)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;   

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox29.
function checkbox29_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox29


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(6)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(6)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox30.
function checkbox30_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox30


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(9)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(9)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
        
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox31.
function checkbox31_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox31

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(12)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(12)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox32.
function checkbox32_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox32


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(11)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(11)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox33.
function checkbox33_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox33


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz4(10)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz4(10)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz4(1,i)==1
        
axes(handles.ACC_L_R);

    plot(handles.time,handles.accx(i+12,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz4)==0 

    cla(handles.ACC_L_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox46.
function checkbox46_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox46 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox46

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz1(1)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');

 
    if handles.niz1(1,i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz1(1)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');  
   
    if handles.niz1(1,i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz1)==0 

    cla(handles.FSR_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);

% --- Executes on button press in checkbox47.
function checkbox47_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox47


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz1(4)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz1(4)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz1)==0 

    cla(handles.FSR_R_R);
    hold on;
    
    end
xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox48.
function checkbox48_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox48 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox48

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz1(3)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz1(3)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz1)==0 

    cla(handles.FSR_R_R);
    hold on;
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;  

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);



% --- Executes on button press in checkbox49.
function checkbox49_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox49 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox49

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz1(2)=1;

for i=1:4
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz1(2)=0;
    
    for i=1:4
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz1(i)==1
        
axes(handles.FSR_R_R);

    plot(handles.time,handles.fsrx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz1)==0 

    cla(handles.FSR_R_R);
    hold on;
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);



% --- Executes on button press in checkbox34.
function checkbox34_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox34


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(1)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(1)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox35.
function checkbox35_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox35


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(4)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(4)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox36.
function checkbox36_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox36 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox36


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(3)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(3)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox37.
function checkbox37_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox37 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox37


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(2)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(2)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox38.
function checkbox38_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox38 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox38


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(5)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(5)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox39.
function checkbox39_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox39


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(8)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(8)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox40.
function checkbox40_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox40

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(7)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(7)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox41.
function checkbox41_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox41

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(6)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(6)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox42.
function checkbox42_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox42 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox42


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(9)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(9)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');    
     
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox43.
function checkbox43_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox43 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox43


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(12)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(12)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);

% --- Executes on button press in checkbox44.
function checkbox44_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox44 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox44

if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(11)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(11)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;    

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in checkbox45.
function checkbox45_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox45


if get(hObject,'Value') == get(hObject,'Max')
    

handles.niz3(10)=1;

for i=1:12
    
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
 
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
end

xlim([0 handles.tstops-handles.tstarts]);

end
hold off;

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');

else

    handles.niz3(10)=0;
    
    for i=1:12
        
    set(handles.checkbox46,'Enable','off');
    set(handles.checkbox49,'Enable','off');
    set(handles.checkbox48,'Enable','off');
    set(handles.checkbox47,'Enable','off');
    set(handles.checkbox5,'Enable','off');
    set(handles.checkbox8,'Enable','off');
    set(handles.checkbox7,'Enable','off');
    set(handles.checkbox6,'Enable','off');
    set(handles.checkbox22,'Enable','off');
    set(handles.checkbox23,'Enable','off');
    set(handles.checkbox24,'Enable','off');
    set(handles.checkbox25,'Enable','off');
    set(handles.checkbox26,'Enable','off');
    set(handles.checkbox27,'Enable','off');
    set(handles.checkbox28,'Enable','off');
    set(handles.checkbox29,'Enable','off');
    set(handles.checkbox30,'Enable','off');
    set(handles.checkbox31,'Enable','off');
    set(handles.checkbox32,'Enable','off');
    set(handles.checkbox33,'Enable','off');
    set(handles.checkbox34,'Enable','off');
    set(handles.checkbox35,'Enable','off');
    set(handles.checkbox36,'Enable','off');
    set(handles.checkbox37,'Enable','off');
    set(handles.checkbox38,'Enable','off');
    set(handles.checkbox39,'Enable','off');
    set(handles.checkbox40,'Enable','off');
    set(handles.checkbox41,'Enable','off');
    set(handles.checkbox42,'Enable','off');
    set(handles.checkbox43,'Enable','off');
    set(handles.checkbox44,'Enable','off');
    set(handles.checkbox45,'Enable','off');
   
    if handles.niz3(1,i)==1
        
axes(handles.ACC_R_R);

    plot(handles.time,handles.accx(i,:),'Color',handles.color3(i,:));
    hold on;
    
    elseif sum(handles.niz3)==0 

    cla(handles.ACC_R_R);
    hold on;
    
    
end

xlim([0 handles.tstops-handles.tstarts]);

end
    
hold off;   

    set(handles.checkbox46,'Enable','on');
    set(handles.checkbox49,'Enable','on');
    set(handles.checkbox48,'Enable','on');
    set(handles.checkbox47,'Enable','on');
    set(handles.checkbox5,'Enable','on');
    set(handles.checkbox8,'Enable','on');
    set(handles.checkbox7,'Enable','on');
    set(handles.checkbox6,'Enable','on');
    set(handles.checkbox22,'Enable','on');
    set(handles.checkbox23,'Enable','on');
    set(handles.checkbox24,'Enable','on');
    set(handles.checkbox25,'Enable','on');
    set(handles.checkbox26,'Enable','on');
    set(handles.checkbox27,'Enable','on');
    set(handles.checkbox28,'Enable','on');
    set(handles.checkbox29,'Enable','on');
    set(handles.checkbox30,'Enable','on');
    set(handles.checkbox31,'Enable','on');
    set(handles.checkbox32,'Enable','on');
    set(handles.checkbox33,'Enable','on');
    set(handles.checkbox34,'Enable','on');
    set(handles.checkbox35,'Enable','on');
    set(handles.checkbox36,'Enable','on');
    set(handles.checkbox37,'Enable','on');
    set(handles.checkbox38,'Enable','on');
    set(handles.checkbox39,'Enable','on');
    set(handles.checkbox40,'Enable','on');
    set(handles.checkbox41,'Enable','on');
    set(handles.checkbox42,'Enable','on');
    set(handles.checkbox43,'Enable','on');
    set(handles.checkbox44,'Enable','on');
    set(handles.checkbox45,'Enable','on');
    
end
guidata(hObject,handles);


% --- Executes on button press in pushbutton8.
function pushbutton8_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton8 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox46,'Value',1);
set(handles.checkbox49,'Value',1);
set(handles.checkbox48,'Value',1);
set(handles.checkbox47,'Value',1);
handles.niz1=ones(1,4);

axes(handles.FSR_R_R);

for j=1:handles.nfsr
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

guidata(hObject,handles);



% --- Executes on button press in pushbutton9.
function pushbutton9_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton9 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox46,'Value',0);
set(handles.checkbox49,'Value',0);
set(handles.checkbox48,'Value',0);
set(handles.checkbox47,'Value',0);
handles.niz1=zeros(1,4);

cla(handles.FSR_R_R);

guidata(hObject,handles);


% --- Executes on button press in pushbutton10.
function pushbutton10_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox5,'Value',1);
set(handles.checkbox8,'Value',1);
set(handles.checkbox7,'Value',1);
set(handles.checkbox6,'Value',1);
handles.niz2=ones(1,4);

axes(handles.FSR_L_R);

for j=5:8
    plot(handles.time,handles.fsrx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

guidata(hObject,handles);


% --- Executes on button press in pushbutton11.
function pushbutton11_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox5,'Value',0);
set(handles.checkbox8,'Value',0);
set(handles.checkbox7,'Value',0);
set(handles.checkbox6,'Value',0);
handles.niz2=zeros(1,4);

cla(handles.FSR_L_R);

guidata(hObject,handles);


% --- Executes on button press in pushbutton12.
function pushbutton12_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox34,'Value',1);
set(handles.checkbox37,'Value',1);
set(handles.checkbox36,'Value',1);
set(handles.checkbox35,'Value',1);
set(handles.checkbox38,'Value',1);
set(handles.checkbox41,'Value',1);
set(handles.checkbox40,'Value',1);
set(handles.checkbox39,'Value',1);
set(handles.checkbox42,'Value',1);
set(handles.checkbox45,'Value',1);
set(handles.checkbox44,'Value',1);
set(handles.checkbox43,'Value',1);
handles.niz3=ones(1,12);

axes(handles.ACC_R_R);

for j=1:handles.accmax/2
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

guidata(hObject,handles);



% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


set(handles.checkbox34,'Value',0);
set(handles.checkbox37,'Value',0);
set(handles.checkbox36,'Value',0);
set(handles.checkbox35,'Value',0);
set(handles.checkbox38,'Value',0);
set(handles.checkbox41,'Value',0);
set(handles.checkbox40,'Value',0);
set(handles.checkbox39,'Value',0);
set(handles.checkbox42,'Value',0);
set(handles.checkbox45,'Value',0);
set(handles.checkbox44,'Value',0);
set(handles.checkbox43,'Value',0);
handles.niz3=zeros(1,12);

cla(handles.ACC_R_R);

guidata(hObject,handles);


% --- Executes on button press in pushbutton14.
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.checkbox22,'Value',1);
set(handles.checkbox25,'Value',1);
set(handles.checkbox24,'Value',1);
set(handles.checkbox23,'Value',1);
set(handles.checkbox26,'Value',1);
set(handles.checkbox29,'Value',1);
set(handles.checkbox28,'Value',1);
set(handles.checkbox27,'Value',1);
set(handles.checkbox30,'Value',1);
set(handles.checkbox33,'Value',1);
set(handles.checkbox32,'Value',1);
set(handles.checkbox31,'Value',1);
handles.niz4=ones(1,12);

axes(handles.ACC_L_R);

for j=(handles.accmax/2+1):handles.accmax
    plot(handles.time,handles.accx(j,:),'Color',handles.color3(j-handles.accmax/2,:));
    hold on;
end
hold off;
xlim([0 handles.tstops-handles.tstarts]);

guidata(hObject,handles);


% --- Executes on button press in pushbutton15.
function pushbutton15_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton15 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


set(handles.checkbox22,'Value',0);
set(handles.checkbox25,'Value',0);
set(handles.checkbox24,'Value',0);
set(handles.checkbox23,'Value',0);
set(handles.checkbox26,'Value',0);
set(handles.checkbox29,'Value',0);
set(handles.checkbox28,'Value',0);
set(handles.checkbox27,'Value',0);
set(handles.checkbox30,'Value',0);
set(handles.checkbox33,'Value',0);
set(handles.checkbox32,'Value',0);
set(handles.checkbox31,'Value',0);
handles.niz4=zeros(1,12);

cla(handles.ACC_L_R);

guidata(hObject,handles);


% --------------------------------------------------------------------
function uipushtool12_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool12 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

     set(handles.Opening_pan,'Visible','off');
     set(handles.Raw_pan,'Visible','off');
     set(handles.statistic_pan,'Visible','off');
     set(handles.processed_pan,'Visible','off');   
     set(handles.Selection_pan,'Visible','off');
     set(handles.filter_pan,'Visible','off');
     set(handles.Proc_signal_rb,'Value',0);
     set(handles.Raw_signal_rb,'Value',0);
     set(handles.Stat_rb,'Value',0);
     set(handles.uipushtool7,'Enable','off');
     set(handles.Statistic,'Enable','off');
     set(handles.Print,'Enable','off');
     set(handles.Statistic_menu,'Enable','off');
     set(handles.uipushtool13,'Enable','off');
     set(handles.uipushtool14,'Enable','off');
     set(handles.Untitled_7,'Enable','off');
     set(handles.Stat_rb,'Enable','off');
     
 pushbutton3_Callback(hObject, eventdata, handles)
     

% --------------------------------------------------------------------
function Untitled_7_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function word_Callback(hObject, eventdata, handles)
% hObject    handle to word (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pushbutton5_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function excel_Callback(hObject, eventdata, handles)
% hObject    handle to excel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pushbutton4_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function changeseq_Callback(hObject, eventdata, handles)
% hObject    handle to changeseq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

uipushtool12_ClickedCallback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Raw_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Raw (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Raw_signal_rb,'Value',1);
Raw_signal_rb_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Processed_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Processed (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Proc_signal_rb,'Value',1);
Proc_signal_rb_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Statistic_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to Statistic (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

set(handles.Stat_rb,'Value',1);
Stat_rb_Callback(hObject, eventdata, handles)



function edit20_Callback(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit20 as text
%        str2double(get(hObject,'String')) returns contents of edit20 as a double


% --- Executes during object creation, after setting all properties.
function edit20_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit20 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function uipushtool13_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pushbutton4_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function uipushtool14_ClickedCallback(hObject, eventdata, handles)
% hObject    handle to uipushtool14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

pushbutton5_Callback(hObject, eventdata, handles)



function edit61_Callback(hObject, eventdata, handles)
% hObject    handle to edit61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit61 as text
%        str2double(get(hObject,'String')) returns contents of edit61 as a double


% --- Executes during object creation, after setting all properties.
function edit61_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit61 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit62_Callback(hObject, eventdata, handles)
% hObject    handle to edit62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit62 as text
%        str2double(get(hObject,'String')) returns contents of edit62 as a double


% --- Executes during object creation, after setting all properties.
function edit62_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit62 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit63_Callback(hObject, eventdata, handles)
% hObject    handle to edit63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit63 as text
%        str2double(get(hObject,'String')) returns contents of edit63 as a double


% --- Executes during object creation, after setting all properties.
function edit63_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit63 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit64_Callback(hObject, eventdata, handles)
% hObject    handle to edit64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit64 as text
%        str2double(get(hObject,'String')) returns contents of edit64 as a double


% --- Executes during object creation, after setting all properties.
function edit64_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit64 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit65_Callback(hObject, eventdata, handles)
% hObject    handle to edit65 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit65 as text
%        str2double(get(hObject,'String')) returns contents of edit65 as a double


% --- Executes during object creation, after setting all properties.
function edit65_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit65 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton16.
%function pushbutton16_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton16 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


guidata(hObject,handles);

% --- Executes on button press in togglebutton1.
function togglebutton1_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton1

if get(hObject,'Value')==get(hObject,'Max')
    
    set(handles.uipanel17,'Visible','off');
    set(handles.uipanel16,'Visible','on');
    set(handles.togglebutton2,'Value',0);
    set(handles.togglebutton2,'Enable','on');
    set(handles.togglebutton1,'Value',1);
    set(handles.togglebutton1,'Enable','inactive');
    
else
    
end

guidata(hObject,handles);


% --- Executes on button press in togglebutton2.
function togglebutton2_Callback(hObject, eventdata, handles)
% hObject    handle to togglebutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of togglebutton2


if get(hObject,'Value')==get(hObject,'Max')
    
    set(handles.uipanel16,'Visible','off');
    set(handles.uipanel17,'Visible','on');
    set(handles.togglebutton1,'Value',0);
    set(handles.togglebutton1,'Enable','on');
    set(handles.togglebutton2,'Value',1);
    set(handles.togglebutton2,'Enable','inactive');
    
else
    
end

guidata(hObject,handles);



function edit21_Callback(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit21 as text
%        str2double(get(hObject,'String')) returns contents of edit21 as a double


% --- Executes during object creation, after setting all properties.
function edit21_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit21 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit22_Callback(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit22 as text
%        str2double(get(hObject,'String')) returns contents of edit22 as a double


% --- Executes during object creation, after setting all properties.
function edit22_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit22 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit23_Callback(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit23 as text
%        str2double(get(hObject,'String')) returns contents of edit23 as a double


% --- Executes during object creation, after setting all properties.
function edit23_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit23 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit24_Callback(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit24 as text
%        str2double(get(hObject,'String')) returns contents of edit24 as a double


% --- Executes during object creation, after setting all properties.
function edit24_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit24 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit25_Callback(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit25 as text
%        str2double(get(hObject,'String')) returns contents of edit25 as a double


% --- Executes during object creation, after setting all properties.
function edit25_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit25 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit26_Callback(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit26 as text
%        str2double(get(hObject,'String')) returns contents of edit26 as a double


% --- Executes during object creation, after setting all properties.
function edit26_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit26 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit27_Callback(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit27 as text
%        str2double(get(hObject,'String')) returns contents of edit27 as a double


% --- Executes during object creation, after setting all properties.
function edit27_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit27 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit28_Callback(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit28 as text
%        str2double(get(hObject,'String')) returns contents of edit28 as a double


% --- Executes during object creation, after setting all properties.
function edit28_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit28 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit29_Callback(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit29 as text
%        str2double(get(hObject,'String')) returns contents of edit29 as a double


% --- Executes during object creation, after setting all properties.
function edit29_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit29 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit30_Callback(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit30 as text
%        str2double(get(hObject,'String')) returns contents of edit30 as a double


% --- Executes during object creation, after setting all properties.
function edit30_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit30 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit31_Callback(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit31 as text
%        str2double(get(hObject,'String')) returns contents of edit31 as a double


% --- Executes during object creation, after setting all properties.
function edit31_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit31 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit32_Callback(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit32 as text
%        str2double(get(hObject,'String')) returns contents of edit32 as a double


% --- Executes during object creation, after setting all properties.
function edit32_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit32 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit57_Callback(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit57 as text
%        str2double(get(hObject,'String')) returns contents of edit57 as a double


% --- Executes during object creation, after setting all properties.
function edit57_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit57 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit58_Callback(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit58 as text
%        str2double(get(hObject,'String')) returns contents of edit58 as a double


% --- Executes during object creation, after setting all properties.
function edit58_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit58 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit59_Callback(hObject, eventdata, handles)
% hObject    handle to edit59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit59 as text
%        str2double(get(hObject,'String')) returns contents of edit59 as a double


% --- Executes during object creation, after setting all properties.
function edit59_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit59 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit60_Callback(hObject, eventdata, handles)
% hObject    handle to edit60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit60 as text
%        str2double(get(hObject,'String')) returns contents of edit60 as a double


% --- Executes during object creation, after setting all properties.
function edit60_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit60 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit33_Callback(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit33 as text
%        str2double(get(hObject,'String')) returns contents of edit33 as a double


% --- Executes during object creation, after setting all properties.
function edit33_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit33 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit34_Callback(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit34 as text
%        str2double(get(hObject,'String')) returns contents of edit34 as a double


% --- Executes during object creation, after setting all properties.
function edit34_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit34 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit35_Callback(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit35 as text
%        str2double(get(hObject,'String')) returns contents of edit35 as a double


% --- Executes during object creation, after setting all properties.
function edit35_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit35 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit39_Callback(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit39 as text
%        str2double(get(hObject,'String')) returns contents of edit39 as a double


% --- Executes during object creation, after setting all properties.
function edit39_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit39 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit40_Callback(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit40 as text
%        str2double(get(hObject,'String')) returns contents of edit40 as a double


% --- Executes during object creation, after setting all properties.
function edit40_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit40 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit41_Callback(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit41 as text
%        str2double(get(hObject,'String')) returns contents of edit41 as a double


% --- Executes during object creation, after setting all properties.
function edit41_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit41 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit45_Callback(hObject, eventdata, handles)
% hObject    handle to edit45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit45 as text
%        str2double(get(hObject,'String')) returns contents of edit45 as a double


% --- Executes during object creation, after setting all properties.
function edit45_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit45 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit47_Callback(hObject, eventdata, handles)
% hObject    handle to edit47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit47 as text
%        str2double(get(hObject,'String')) returns contents of edit47 as a double


% --- Executes during object creation, after setting all properties.
function edit47_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit47 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit51_Callback(hObject, eventdata, handles)
% hObject    handle to edit51 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit51 as text
%        str2double(get(hObject,'String')) returns contents of edit51 as a double


% --- Executes during object creation, after setting all properties.
function edit51_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit51 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit53_Callback(hObject, eventdata, handles)
% hObject    handle to edit53 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit53 as text
%        str2double(get(hObject,'String')) returns contents of edit53 as a double


% --- Executes during object creation, after setting all properties.
function edit53_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit53 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
