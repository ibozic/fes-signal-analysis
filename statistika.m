

function [Step_Count,Ambulation_Time,Cadence,Step_Time_Differential_Av,Cycle_Time_Differential_Av,Step_Time_Right_Av,Step_Time_Left_Av,Cycle_Time_Right_Av,Cycle_Time_Left_Av,Swing_Time_Right_Av,Swing_Time_Left_Av,Stance_Time_Right_Av,Stance_Time_Left_Av,Single_Supp_Time_Right_Av,Single_Supp_Time_Left_Av,Double_Support_Time_Right_Av,Double_Support_Time_Left_Av,Swing_Cycle_Right_Av,Swing_Cycle_Left_Av,Stance_Cycle_Right_Av,Stance_Cycle_Left_Av,Single_Supp_Cycle_Right_Av,Single_Supp_Cycle_Left_Av,Double_Support_Cycle_Right_Av,Double_Support_Cycle_Left_Av,Heel_Off_On_Time_Right_Av,Heel_Off_On_Time_Left_Av,Step_Time_Right_Std,Step_Time_Left_Std,Stride_Time_Right_Std,Stride_Time_Left_Std,Swing_Time_Right_Std,Swing_Time_Left_Std,Stance_Time_Right_Std,Stance_Time_Left_Std,Single_Support_Right_Std,Single_Support_Left_Std,Double_Support_Time_Right_Std,Double_Support_Time_Left_Std,Heel_Off_On_Right_Std,Heel_Off_On_Left_Std,Heel_Off_On_Cycle_Av_L,Heel_Off_On_Cycle_Av_R] = statistika(chdata,istartv,istopv,nsamples,tstartv,tstopv)


%   detection parameters

    fsrlimit=0.1;     % threshold for foot-contact detection by normalized FSRs; fsrlimit=0.1 is standard, fsrlimit>=1 excludes FSRs;

    avestart=16;    % number of averaging samples for initial autocalibration

    avequiet=10;     % number of samples for foot-contact detection by ACCs (length of quiet zone)
    averange=0.25;   % peak variation of ACC signals (in m/s^2) within quiet zone (1 quantum at 10 bits/sample = 0.1 m/s^2)

    avsqfjump=50;    % maximal abrupt change of squared angular velocity (clipping limit) for foot
    avsqfnoise=1.25;  % noise floor of squared angular velocity (in (rad/s)^2) for foot (1 quantum at 10 bits/sample = 1.2 (rad/s)^2)
    avsqsjump=50;    % maximal abrupt change of squared angular velocity (clipping limit) for shank
    avsqsnoise=1.25;  % noise floor of squared angular velocity for shank
    avsqtjump=50;    % maximal abrupt change of squared angular velocity (clipping limit) for thigh
    avsqtnoise=1.25;  % noise floor of squared angular velocity for thigh

%   other parameters

    g=9.806;        % gravitational acceleration
    length=0.076;   % distance between ACCs on a rod



fsrmtt=1;
lrswap=0;
% if strncmp(file,'Jeca',4)
%     fsrmtt=0;
%     lrswap=1;
% end
% 
% if strncmp(file,'Marija',6)
%     fsrmtt=0;
% end

%   initialize data for samples

fsample=1000/6;     % buda sampling frequency
tstep=1/fsample;    % sampling period
maxsamples=20000;   % maximal number of samples



n_sensor_channels=34;

%   define time instants

% time=0:1:nsamples-1;
% time=time*tstep;
% time_all=time;

%   8 FSR channels: 1-8

if lrswap==1    % swap channels to correct misplaced FSRs
    for j=1:4
        tmpfsrt=chdata(j,:);
        chdata(j,:)=chdata(j+4,:);
        chdata(j+4,:)=tmpfsrt;
    end
end

if fsrmtt==0    % delete saturated FSR signal
    chdata(1,:)=0*chdata(1,:);
end



%   combine FSR channels

maxfsr=2;
fsr(1,:)=chdata(4,:);
fsr(2,:)=(chdata(1,:)+chdata(2,:)+chdata(3,:))/3;
fsr(3,:)=chdata(8,:); 
fsr(4,:)=(chdata(5,:)+chdata(6,:)+chdata(7,:))/3;

fsrmin=0;
fsrmax=1000;


istart=1;
istop=nsamples;
nwork=istop-istart+1;


%   Process RIGHT leg


for j=1:maxfsr
    k=0;
    for i=istart:istop
        k=k+1;
        seq(j,k)=fsr(j,i);
    end
end

%   order signals according to their peak intensities

for j=1:maxfsr
	peak(j)=0;
	for i=1:nwork
		if(abs(seq(j,i))>peak(j))
            peak(j)=abs(seq(j,i));
        end
    end
	peaktmp(j)=peak(j);
end
for j=1:maxfsr
	peakorder(j)=j;
end
for j=1:maxfsr-1
	for i=j+1:maxfsr
		if(peaktmp(j)<peaktmp(i))
			temp=peaktmp(j);
			peaktmp(j)=peaktmp(i);
			peaktmp(i)=temp;
			itemp=peakorder(j);
			peakorder(j)=peakorder(i);
			peakorder(i)=itemp;
        end
    end
end

%	correlation analysis

%	autocorrelation

for j=1:maxfsr
	for i=1:nwork
		seqauxa(i)=seq(j,i);
    end
    seqcorrauxa=xcorr(seqauxa);
	for i=1:2*nwork-1
		seqcorra(j,i)=seqcorrauxa(i);
    end
end

%	estimation of the period from strongest signals

syncch=2;
period=0;
for j=1:syncch
	periodestimate(j)=0;
	temp=0;
	for i=round(0.8/tstep):round(2/tstep)  %   period se trazi izmedju 1 i 2.5 sekunde
		if(temp<seqcorra(peakorder(j),i+nwork))
            temp=seqcorra(peakorder(j),i+nwork);
			periodestimate(j)=i*tstep;
        end
	end
	period=period+periodestimate(j);
end
period=period/syncch;

%	korelacija sa dugim impulsom (odredjivanje medijana uzastopnih pauza)

nfilter=round(0.15*period/tstep)-1;
nfilterh=round(nfilter/2);
for i=1:nfilter
    seqpeakw(i)=1;
end

for i=1:nwork
    seqave(i)=0;
    for j=1:maxfsr
        seqave(i)=seqave(i)+seq(j,i);
    end
    seqave(i)=seqave(i)/maxfsr;
end
        
for j=1:maxfsr
	for i=1:nwork
		seqauxw(i)=seq(j,i);
    end
    seqcorrauxw=xcorr(seqauxw,seqpeakw);
    for i=1:nwork+nfilter-1
		seqcorrw(j,i)=seqcorrauxw(i+nwork-nfilter);
    end
end

seqcorrauxw=xcorr(seqave,seqpeakw);
for i=1:nwork+nfilter-1
	seqcorrw(maxfsr+1,i)=seqcorrauxw(i+nwork-nfilter);
end

for j=1:maxfsr+1
	for i=1:nwork
		seqsmoothw(j,i)=seqcorrw(j,i+nfilterh)/double(nfilter);
    end
end

%	odredjivanje medijana i mrtvih zona

npause=round(nwork/(period/tstep));
ileft=nfilter+1;
iright=round(double(ileft)+1.2*period/tstep);

for i=1:npause
 
	if(iright>nwork-1)
        npause=i-1;
        break;
    end
  
    for j=1:maxfsr+1
		valley(j,i)=1.e10;
		ivalley=ileft;
		for k=ileft:iright
			if(valley(j,i)>seqsmoothw(j,k))
				valley(j,i)=seqsmoothw(j,k);
				ivalley=k;
            end
        end
		pauseestimate(j)=double(ivalley)*tstep;
    end

    median(i)=pauseestimate(maxfsr+1);
	ivalley=round(median(i)/tstep);
	ileft=round(ivalley+0.6*period/tstep);
	iright=round(double(ileft)+0.8*period/tstep)-1;

end

%	odredjivanje perioda na osnovu medijana

for i=1:npause-1
	periodestimate(i)=median(i+1)-median(i);
end

period=0;
for i=1:npause-1
	period=period+periodestimate(i);
end
period=period/double(npause-1);
periodjitter=0.;
for i=1:npause-1
	periodjitter=periodjitter+(periodestimate(i)-period)^2;
end
periodjitter=sqrt(periodjitter/double(npause-1));

%fprintf(1,'\n Period na osnovu medijana %g s, jitter %g s',period,periodjitter);

%	korelacija sa kratkim impulsom (usrednjavanje)

nfilters=11;
nfiltersh=round(nfilters/2);
for i=1:nfilters
    seqpeaks(i)=1;
end
for j=1:maxfsr
	for i=1:nwork
		seqauxs(i)=seq(j,i);
    end
    seqcorrauxs=xcorr(seqauxs,seqpeaks);
    for i=1:nwork+nfilters-1
		seqcorrs(j,i)=seqcorrauxs(i+nwork-nfilters-1);
    end
end

for j=1:maxfsr
	pknorms(j)=0;
	for i=1:nwork+nfilters-1
		if(abs(seqcorrs(j,i))>pknorms(j))
            pknorms(j)=abs(seqcorrs(j,i));
        end
    end
end

for j=1:maxfsr
	for i=1:nwork
		seqsmooths(j,i)=seqcorrs(j,i+nfiltersh)/double(nfilters);
    end
end


%	odredjivanje pocetka, kraja i vrha impulsa
%	vremena su relativna u odnosu na prethodnu medijanu

%   npause=round(nwork/(period/tstep));

for j=1:maxfsr
	for i=1:npause-1
		ileft=round(median(i)/tstep);
        if(ileft<1)
            ileft=1;
        end
		iright=round(median(i+1)/tstep);
        if(iright>nwork)
            npause=npause-1;
            break;
        end          
		vpeak(j,i)=0;
		ipeak=ileft;
		for k=ileft:iright
			if(vpeak(j,i)<seqsmooths(j,k))
				vpeak(j,i)=seqsmooths(j,k);
				ipeak=k;
            end
        end
		tpeak(j,i)=double(ipeak-ileft)*tstep;
        xmark=double(ipeak)*tstep;
        ymark=vpeak(j,i);
%         plot(xmark,ymark,'--ro','MarkerEdgeColor','k','MarkerFaceColor',color3(j,:),'MarkerSize',5)
%         hold on;

%	odredjivanje pocetka i kraja impulsa na 10% vrsne vrednosti

        temp=0.1*vpeak(j,i)+0.45*(valley(j,i)+valley(j,i+1));

        iedgestart=iright;
        for k=ileft:iright
            if(seqsmooths(j,k)>temp)
                iedgestart=k;
                break
            end
        end
        tstart(j,i)=double(iedgestart-ileft)*tstep;
        xmark=double(iedgestart)*tstep;
        tdownR(j,i)=xmark;
        ymark=temp;
%         plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(j,:),'MarkerSize',5)
%         hold on;

        iedgestop=ileft;
        for k=iright:-1:ileft
            if(seqsmooths(j,k)>temp)
                iedgestop=k;
                break
            end
        end
        tstop(j,i)=double(iedgestop-ileft)*tstep;
        xmark=double(iedgestop)*tstep;
        tupR(j,i)=xmark;
        ymark=temp;
%         plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(j,:),'MarkerSize',5)
%         hold on;

    end 
end

for i=1:npause
    xmark=median(i);
    ymark=0;
%     plot(xmark,ymark,'--rs','MarkerEdgeColor','k','MarkerFaceColor','c','MarkerSize',6)
%     hold on;
end

istride=0;
for i=1:npause-1
    if median(i)>tstartv
        if median(i+1)<tstopv
            xmark=min(tdownR(:,i));
            ymark=0;
%             plot(xmark,ymark,'--rv','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',6)
%             hold on;
%             fprintf(fidw,'%8.3f    DOWN \n',xmark);
            itemp1=round(xmark/tstep);
            xmark=max(tupR(:,i));
            ymark=0;
%             plot(xmark,ymark,'--r^','MarkerEdgeColor','k','MarkerFaceColor','b','MarkerSize',6)
%             hold on;
%             fprintf(fidw,'%8.3f     UP \n',xmark);
            itemp2=round(xmark/tstep);
            istride=istride+1;
            istillright(istride)=round(0.5*(itemp1+itemp2));
        end
    end
end

nstrideright=istride;

% hold off;
% fclose(fidw);


%   Process LEFT leg

% filew=strrep(file,'.gaz','_left.txt');
% if strcmp(filew,file)
%     filew='left.txt';
% end
% fidw=fopen(filew,'w');  
% fprintf(fidw,'time [s]    event \n');
% fprintf(fidw,'\n');

for j=1:maxfsr
    k=0;
    for i=istart:istop
        k=k+1;
        seq(j,k)=fsr(j+maxfsr,i);
    end
end

%   order signals according to their peak intensities

for j=1:maxfsr
	peak(j)=0;
	for i=1:nwork
		if(abs(seq(j,i))>peak(j))
            peak(j)=abs(seq(j,i));
        end
    end
	peaktmp(j)=peak(j);
end
for j=1:maxfsr
	peakorder(j)=j;
end
for j=1:maxfsr-1
	for i=j+1:maxfsr
		if(peaktmp(j)<peaktmp(i))
			temp=peaktmp(j);
			peaktmp(j)=peaktmp(i);
			peaktmp(i)=temp;
			itemp=peakorder(j);
			peakorder(j)=peakorder(i);
			peakorder(i)=itemp;
        end
    end
end

%	correlation analysis

%	autocorrelation

for j=1:maxfsr
	for i=1:nwork
		seqauxa(i)=seq(j,i);
    end
    seqcorrauxa=xcorr(seqauxa);
	for i=1:2*nwork-1
		seqcorra(j,i)=seqcorrauxa(i);
    end
end

%	estimation of the period from strongest signals

syncch=2;
period=0;
for j=1:syncch
	periodestimate(j)=0;
	temp=0;
	for i=round(0.8/tstep):round(2/tstep)  %   period se trazi izmedju 1 i 2.5 sekunde
		if(temp<seqcorra(peakorder(j),i+nwork))
            temp=seqcorra(peakorder(j),i+nwork);
			periodestimate(j)=i*tstep;
        end
	end
	period=period+periodestimate(j);
end
period=period/syncch;

%	korelacija sa dugim impulsom (odredjivanje medijana uzastopnih pauza)

nfilter=round(0.15*period/tstep)-1;
nfilterh=round(nfilter/2);
for i=1:nfilter
    seqpeakw(i)=1;
end

for i=1:nwork
    seqave(i)=0;
    for j=1:maxfsr
        seqave(i)=seqave(i)+seq(j,i);
    end
    seqave(i)=seqave(i)/maxfsr;
end
        
for j=1:maxfsr
	for i=1:nwork
		seqauxw(i)=seq(j,i);
    end
    seqcorrauxw=xcorr(seqauxw,seqpeakw);
    for i=1:nwork+nfilter-1
		seqcorrw(j,i)=seqcorrauxw(i+nwork-nfilter);
    end
end

seqcorrauxw=xcorr(seqave,seqpeakw);
for i=1:nwork+nfilter-1
	seqcorrw(maxfsr+1,i)=seqcorrauxw(i+nwork-nfilter);
end

for j=1:maxfsr+1
	for i=1:nwork
		seqsmoothw(j,i)=seqcorrw(j,i+nfilterh)/double(nfilter);
    end
end

%	odredjivanje medijana i mrtvih zona

npause=round(nwork/(period/tstep));
ileft=nfilter+1;
iright=round(double(ileft)+1.2*period/tstep);

for i=1:npause
 
	if(iright>nwork-1)
        npause=i-1;
        break;
    end
  
    for j=1:maxfsr+1
		valley(j,i)=1.e10;
		ivalley=ileft;
		for k=ileft:iright
			if(valley(j,i)>seqsmoothw(j,k))
				valley(j,i)=seqsmoothw(j,k);
				ivalley=k;
            end
        end
		pauseestimate(j)=double(ivalley)*tstep;
    end

    median(i)=pauseestimate(maxfsr+1);
	ivalley=round(median(i)/tstep);
	ileft=round(ivalley+0.6*period/tstep);
	iright=round(double(ileft)+0.8*period/tstep)-1;

end

%	odredjivanje perioda na osnovu medijana

for i=1:npause-1
	periodestimate(i)=median(i+1)-median(i);
end

period=0;
for i=1:npause-1
	period=period+periodestimate(i);
end
period=period/double(npause-1);
periodjitter=0.;
for i=1:npause-1
	periodjitter=periodjitter+(periodestimate(i)-period)^2;
end
periodjitter=sqrt(periodjitter/double(npause-1));

%fprintf(1,'\n Period na osnovu medijana %g s, jitter %g s',period,periodjitter);

%	korelacija sa kratkim impulsom (usrednjavanje)

nfilters=9;
nfiltersh=round(nfilters/2);
for i=1:nfilters
    seqpeaks(i)=1;
end
for j=1:maxfsr
	for i=1:nwork
		seqauxs(i)=seq(j,i);
    end
    seqcorrauxs=xcorr(seqauxs,seqpeaks);
    for i=1:nwork+nfilters-1
		seqcorrs(j,i)=seqcorrauxs(i+nwork-nfilters-1);
    end
end

for j=1:maxfsr
	pknorms(j)=0;
	for i=1:nwork+nfilters-1
		if(abs(seqcorrs(j,i))>pknorms(j))
            pknorms(j)=abs(seqcorrs(j,i));
        end
    end
end

for j=1:maxfsr
	for i=1:nwork
		seqsmooths(j,i)=seqcorrs(j,i+nfiltersh)/double(nfilters);
    end
end


% axis([0 tstep*nwork fsrmin fsrmax])
%	odredjivanje pocetka, kraja i vrha impulsa
%	vremena su relativna u odnosu na prethodnu medijanu

%   npause=round(nwork/(period/tstep));

for j=1:maxfsr
	for i=1:npause-1
		ileft=round(median(i)/tstep);
        if(ileft<1)
            ileft=1;
        end
		iright=round(median(i+1)/tstep);
        if(iright>nwork)
            npause=npause-1;
            break;
        end          
		vpeak(j,i)=0;
		ipeak=ileft;
		for k=ileft:iright
			if(vpeak(j,i)<seqsmooths(j,k))
				vpeak(j,i)=seqsmooths(j,k);
				ipeak=k;
            end
        end
		tpeak(j,i)=double(ipeak-ileft)*tstep;
        xmark=double(ipeak)*tstep;
        ymark=vpeak(j,i);
%         plot(xmark,ymark,'--ro','MarkerEdgeColor','k','MarkerFaceColor',color3(j+4,:),'MarkerSize',5)
%         hold on;

%	odredjivanje pocetka i kraja impulsa na 10% vrsne vrednosti

        temp=0.1*vpeak(j,i)+0.45*(valley(j,i)+valley(j,i+1));

        iedgestart=iright;
        for k=ileft:iright
            if(seqsmooths(j,k)>temp)
                iedgestart=k;
                break
            end
        end
        tstart(j,i)=double(iedgestart-ileft)*tstep;
        xmark=double(iedgestart)*tstep;
        tdownL(j,i)=xmark;
        ymark=temp;
%         plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(j+4,:),'MarkerSize',5)
%         hold on;

        iedgestop=ileft;
        for k=iright:-1:ileft
            if(seqsmooths(j,k)>temp)
                iedgestop=k;
                break
            end
        end
        tstop(j,i)=double(iedgestop-ileft)*tstep;
        xmark=double(iedgestop)*tstep;
        tupL(j,i)=xmark;
        ymark=temp;
%         plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(j+4,:),'MarkerSize',5)
%         
%         hold on;

    end 
end

for i=1:npause
    xmark=median(i);
    ymark=0;
%     plot(xmark,ymark,'--rs','MarkerEdgeColor','k','MarkerFaceColor','c','MarkerSize',6)
%     hold on;
end

istride=0;
for i=1:npause-1
    if median(i)>tstartv
        if median(i+1)<tstopv
            xmark=min(tdownL(:,i));
            ymark=0;
%             plot(xmark,ymark,'--rv','MarkerEdgeColor','k','MarkerFaceColor','r','MarkerSize',6)
%             hold on;
%             fprintf(fidw,'%8.3f    DOWN \n',xmark);
            itemp1=round(xmark/tstep);
            xmark=max(tupL(:,i));
            ymark=0;
%             plot(xmark,ymark,'--r^','MarkerEdgeColor','k','MarkerFaceColor','b','MarkerSize',6)
%             hold on;
%             fprintf(fidw,'%8.3f     UP \n',xmark);
            itemp2=round(xmark/tstep);
            istride=istride+1;
            istillleft(istride)=round(0.5*(itemp1+itemp2));
        end
    end
end

nstrideleft=istride;

% fclose(fidw);

tsize=size(tupR);
sizeupR=tsize(2);
for i=1:sizeupR
    tupRt(i)=max(tupR(:,i));
end

tsize=size(tdownR);
sizedownR=tsize(2);
for i=1:sizedownR
    tdownRt(i)=min(tdownR(:,i));
end

tsize=size(tupL);
sizeupL=tsize(2);
for i=1:sizeupL
    tupLt(i)=max(tupL(:,i));
end

tsize=size(tdownL);
sizedownL=tsize(2);
for i=1:sizedownL
    tdownLt(i)=min(tdownL(:,i));
end

nstride=0;
for i=1:sizedownR
    if tdownRt(i)>tstartv
        if tdownRt(i)<tstopv
            if nstride==0
                nstartR=i;
                tstartR=tdownRt(i);
            end
            nstride=nstride+1;
        end
    end
end
if nstride<3
%     fprintf('Insufficient number of strides for right leg\n');
%     return

Step_Count=0;
Ambulation_Time =0;
Cadence=0;
Step_Time_Differential_Av=0;
Cycle_Time_Differential_Av=0;
Step_Time_Right_Av=0;
Step_Time_Left_Av=0;
Cycle_Time_Right_Av=0;
Cycle_Time_Left_Av=0;
Swing_Time_Right_Av=0;
Swing_Time_Left_Av=0;
Stance_Time_Right_Av=0;
Stance_Time_Left_Av=0;
Single_Supp_Time_Right_Av=0;
Single_Supp_Time_Left_Av=0;
Double_Support_Time_Right_Av=0;
Double_Support_Time_Left_Av=0;
Swing_Cycle_Right_Av=0;
Swing_Cycle_Left_Av=0;
Stance_Cycle_Right_Av=0;
Stance_Cycle_Left_Av=0;
Single_Supp_Cycle_Right_Av=0;
Single_Supp_Cycle_Left_Av=0;
Double_Support_Cycle_Right_Av=0;
Double_Support_Cycle_Left_Av=0;
Heel_Off_On_Time_Right_Av=0;
Heel_Off_On_Time_Left_Av=0;
Step_Time_Right_Std=0;
Step_Time_Left_Std=0;
Stride_Time_Right_Std=0;
Stride_Time_Left_Std=0;
Swing_Time_Right_Std=0;
Swing_Time_Left_Std=0;
Stance_Time_Right_Std=0;
Stance_Time_Left_Std=0;
Single_Support_Right_Std=0;
Single_Support_Left_Std=0;
Double_Support_Time_Right_Std=0;
Double_Support_Time_Left_Std=0;
Heel_Off_On_Right_Std=0;
Heel_Off_On_Left_Std=0;
Heel_Off_On_Cycle_Av_L=0;
Heel_Off_On_Cycle_Av_R=0;

else
tstopR=tdownRt(nstride);
nstride=nstride-1;

%tdownRt(nstartR:nstartR+nstride);

if nstride<1
    return
end

SwingTimeR=0;
SwingTimeL=0;
StanceTimeR=0;
StanceTimeL=0;
for i=nstartR:nstartR+nstride-1
%     fprintf('Stride # %i\n\n',i-nstartR+1);
%     fprintf('Time %8.3f s: right foot  ON, ',tdownRt(i));
%     if tdownR(1,i)<tdownR(2,i)
%         fprintf('heel first, ');
%     else
%         fprintf('toes first, ');
%     end
%     fprintf('difference %8.3f s\n',abs(tdownR(1,i)-tdownR(2,i)));

    for j=1:sizedownL
        if tupRt(j)>tdownRt(i)
            break;
        end
    end
%     fprintf('Time %8.3f s: right foot OFF, ',tupRt(j));
%     if tupR(1,j)<tupR(2,j)
%         fprintf('heel first, ');
%     else
%         fprintf('toes first, ');
%     end
%     fprintf('difference %8.3f s\n',abs(tupR(1,j)-tupR(2,j)));
%     fprintf('Stance %8.3f s\n',abs(tupRt(j)-tdownRt(i)));
%     fprintf('Swing  %8.3f s\n',abs(tdownRt(i+1)-tupRt(j)));
%     fprintf('Stride %8.3f s\n',abs(tdownRt(i+1)-tdownRt(i)));
    SwingTimeR=SwingTimeR+abs(tdownRt(i+1)-tupRt(j));
    StanceTimeR=StanceTimeR+abs(tupRt(j)-tdownRt(i));
 
%     fprintf('\n');
    for k=1:sizeupL
        if tupLt(k)>tdownRt(i)
            break;
        end
    end
%     fprintf('Time %8.3f s:  left foot OFF, ',tupLt(k));
%     if tupL(1,k)<tupL(2,k)
%         fprintf('heel first, ');
%     else
%         fprintf('toes first, ');
%     end
%     fprintf('difference %8.3f s\n',abs(tupL(1,k)-tupL(2,k)));
    if i<nstartR+nstride-1
        for l=1:sizeupL
            if tdownLt(l)>tdownRt(i)
                break;
            end
        end
% %         fprintf('Time %8.3f s:  left foot  ON, ',tdownLt(l));
%         if tdownL(1,l)<tdownL(2,l)
%             fprintf('heel first, ');
%         else
%             fprintf('toes first, ');
%         end
%         fprintf('difference %8.3f s\n',abs(tdownL(1,l)-tdownL(2,l)));
%         fprintf('Swing  %8.3f s\n',abs(tdownLt(l)-tupLt(k)));
%         fprintf('Stance %8.3f s\n',abs(tupLt(k+1)-tdownLt(l)));
%         fprintf('Stride %8.3f s\n',abs(tupLt(l+1)-tupLt(l)));
        SwingTimeL=SwingTimeL+abs(tdownLt(l)-tupLt(k));
        StanceTimeL=StanceTimeL+abs(tupLt(k+1)-tdownLt(l));
    end

%     fprintf('\n');
%     fprintf('Biped 1 %8.3f s\n',abs(tupLt(k)-tdownRt(i)));
%     if i<nstartR+nstride-1
%         fprintf('Biped 2 %8.3f s\n',abs(tupRt(j)-tdownLt(l)));
%     end
% 
%     fprintf('\n\n');
end

i=nstartR+nstride;
% fprintf('Stride # %i\n\n',i-nstartR+1);
% fprintf('Time %8.3f s: right foot  ON, ',tdownRt(i));
% if tdownR(1,i)<tdownR(2,i)
%     fprintf('heel first, ');
% else
%     fprintf('toes first, ');
% end
% 
% fprintf('\n');
% fprintf('\n');
% fprintf('Step Count %i\n',2*nstride);
AmbulationTime=tdownRt(i)-tdownRt(nstartR);
% fprintf('Ambulation Time %8.3f s\n',AmbulationTime);
% fprintf('Cadence %8.3f steps/min\n',StepCount*60/AmbulationTime);
% fprintf('Step Time %8.3f s\n',0.5*AmbulationTime/nstride);
% fprintf('Cycle Time %8.3f s\n',AmbulationTime/nstride);
% fprintf('Swing Time Right %8.3f s\n',SwingTimeR/nstride);
% if nstride>1 
%     fprintf('Swing Time Left %8.3f s\n',SwingTimeL/(nstride-1));
% end
% fprintf('Stance Time Right %8.3f s\n',StanceTimeR/nstride);
% if nstride>1 
%     fprintf('Stance Time Left %8.3f s\n',StanceTimeL/(nstride-1));
% end

% *************************************************************************

temp_size=size(fsr);
nsamples=temp_size(2);


for j=1:4
    fsr(j,:)=fsr(j,:)-min(fsr(j,:));
    fsr(j,:)=fsr(j,:)/max(fsr(j,:));
end




tdownheelR=tdownR(1,:);
tdowntoesR=tdownR(2,:);
temp_size=size(tdownheelR);
size_tdownR=temp_size(2);

for i=1:size_tdownR
    xmark=tdownheelR(i);
    itime=round(xmark/tstep);
    ymark=fsr(1,itime);
%     plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(1,:),'MarkerSize',5)
    xmark=tdowntoesR(i);
    itime=round(xmark/tstep);
    ymark=fsr(2,itime);
%     plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(3,:),'MarkerSize',5)
end

tupheelR=tupR(1,:);
tuptoesR=tupR(2,:);
temp_size=size(tupheelR);
size_tupR=temp_size(2);

for i=1:size_tupR
    xmark=tupheelR(i);
    itime=round(xmark/tstep);
    ymark=fsr(1,itime);
%     plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(1,:),'MarkerSize',5)
    xmark=tuptoesR(i);
    itime=round(xmark/tstep);
    ymark=fsr(2,itime);
%     plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(3,:),'MarkerSize',5)
end

tdownheelL=tdownL(1,:);
tdowntoesL=tdownL(2,:);
temp_size=size(tdownheelL);
size_tdownL=temp_size(2);

for i=1:size_tdownL
    xmark=tdownheelL(i);
    itime=round(xmark/tstep);
    ymark=fsr(3,itime);
%     plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(5,:),'MarkerSize',5)
    xmark=tdowntoesL(i);
    itime=round(xmark/tstep);
    ymark=fsr(4,itime);
%     plot(xmark,ymark,'--r>','MarkerEdgeColor','k','MarkerFaceColor',color3(7,:),'MarkerSize',5)
end

tupheelL=tupL(1,:);
tuptoesL=tupL(2,:);
temp_size=size(tupheelL);
size_tupL=temp_size(2);

for i=1:size_tupL
    xmark=tupheelL(i);
    itime=round(xmark/tstep);
    ymark=fsr(3,itime);
%     plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(5,:),'MarkerSize',5)
    xmark=tuptoesL(i);
    itime=round(xmark/tstep);
    ymark=fsr(4,itime);
%     plot(xmark,ymark,'--r<','MarkerEdgeColor','k','MarkerFaceColor',color3(7,:),'MarkerSize',5)
end


if tdownheelR(1)>tstartv
    index_start_downheelR=1;
else
    for i=1:size_tdownR
        xmark=tdownheelR(i);
        if xmark<tstartv
            index_start_downheelR=i;
        end
    end
    index_start_downheelR=index_start_downheelR+1;
end
    
if tdownheelL(1)>tstartv
    index_start_downheelL=1;
else
    for i=1:size_tdownL
        xmark=tdownheelL(i);
        if xmark<tstartv
            index_start_downheelL=i;
        end
    end
    index_start_downheelL=index_start_downheelL+1;
end

if tdownheelR(size_tdownR)<tstopv
    index_stop_downheelR=size_tdownR;
else
    for i=size_tdownR:-1:1
        xmark=tdownheelR(i);
        if xmark>tstopv
            index_stop_downheelR=i;
        end
    end
    index_stop_downheelR=index_stop_downheelR-1;
end

if tdownheelL(size_tdownL)<tstopv
    index_stop_downheelL=size_tdownL;
else
    for i=size_tdownL:-1:1
        xmark=tdownheelL(i);
        if xmark>tstopv
            index_stop_downheelL=i;
        end
    end
    index_stop_downheelL=index_stop_downheelL-1;
end

StepCount=index_stop_downheelR-index_start_downheelR+index_stop_downheelL-index_start_downheelL+1;
StrideCountRight=index_stop_downheelR-index_start_downheelR;
StrideCountLeft=index_stop_downheelL-index_start_downheelL;

if StrideCountRight<2
%     fprintf('Insufficient number of strides for right leg\n');
%     return

Step_Count=0;
Ambulation_Time =0;
Cadence=0;
Step_Time_Differential_Av=0;
Cycle_Time_Differential_Av=0;
Step_Time_Right_Av=0;
Step_Time_Left_Av=0;
Cycle_Time_Right_Av=0;
Cycle_Time_Left_Av=0;
Swing_Time_Right_Av=0;
Swing_Time_Left_Av=0;
Stance_Time_Right_Av=0;
Stance_Time_Left_Av=0;
Single_Supp_Time_Right_Av=0;
Single_Supp_Time_Left_Av=0;
Double_Support_Time_Right_Av=0;
Double_Support_Time_Left_Av=0;
Swing_Cycle_Right_Av=0;
Swing_Cycle_Left_Av=0;
Stance_Cycle_Right_Av=0;
Stance_Cycle_Left_Av=0;
Single_Supp_Cycle_Right_Av=0;
Single_Supp_Cycle_Left_Av=0;
Double_Support_Cycle_Right_Av=0;
Double_Support_Cycle_Left_Av=0;
Heel_Off_On_Time_Right_Av=0;
Heel_Off_On_Time_Left_Av=0;
Step_Time_Right_Std=0;
Step_Time_Left_Std=0;
Stride_Time_Right_Std=0;
Stride_Time_Left_Std=0;
Swing_Time_Right_Std=0;
Swing_Time_Left_Std=0;
Stance_Time_Right_Std=0;
Stance_Time_Left_Std=0;
Single_Support_Right_Std=0;
Single_Support_Left_Std=0;
Double_Support_Time_Right_Std=0;
Double_Support_Time_Left_Std=0;
Heel_Off_On_Right_Std=0;
Heel_Off_On_Left_Std=0;
Heel_Off_On_Cycle_Av_L=0;
Heel_Off_On_Cycle_Av_R=0;

else
    if StrideCountLeft<2
%     fprintf('Insufficient number of strides for left leg\n');
%     return

Step_Count=0;
Ambulation_Time =0;
Cadence=0;
Step_Time_Differential_Av=0;
Cycle_Time_Differential_Av=0;
Step_Time_Right_Av=0;
Step_Time_Left_Av=0;
Cycle_Time_Right_Av=0;
Cycle_Time_Left_Av=0;
Swing_Time_Right_Av=0;
Swing_Time_Left_Av=0;
Stance_Time_Right_Av=0;
Stance_Time_Left_Av=0;
Single_Supp_Time_Right_Av=0;
Single_Supp_Time_Left_Av=0;
Double_Support_Time_Right_Av=0;
Double_Support_Time_Left_Av=0;
Swing_Cycle_Right_Av=0;
Swing_Cycle_Left_Av=0;
Stance_Cycle_Right_Av=0;
Stance_Cycle_Left_Av=0;
Single_Supp_Cycle_Right_Av=0;
Single_Supp_Cycle_Left_Av=0;
Double_Support_Cycle_Right_Av=0;
Double_Support_Cycle_Left_Av=0;
Heel_Off_On_Time_Right_Av=0;
Heel_Off_On_Time_Left_Av=0;
Step_Time_Right_Std=0;
Step_Time_Left_Std=0;
Stride_Time_Right_Std=0;
Stride_Time_Left_Std=0;
Swing_Time_Right_Std=0;
Swing_Time_Left_Std=0;
Stance_Time_Right_Std=0;
Stance_Time_Left_Std=0;
Single_Support_Right_Std=0;
Single_Support_Left_Std=0;
Double_Support_Time_Right_Std=0;
Double_Support_Time_Left_Std=0;
Heel_Off_On_Right_Std=0;
Heel_Off_On_Left_Std=0;
Heel_Off_On_Cycle_Av_L=0;
Heel_Off_On_Cycle_Av_R=0;
 
    else 

%   another algorithm for step count

StepCount=-1;
for i=1:size_tdownR
    xmark=tdownheelR(i);
    if xmark>tstartv
        if xmark<tstopv
            StepCount=StepCount+1;
        end
    end
end
for i=1:size_tdownL
    xmark=tdownheelL(i);
    if xmark>tstartv
        if xmark<tstopv
            StepCount=StepCount+1;
        end
    end
end


%   which foot touches first?

if tdownheelR(index_start_downheelR)<tdownheelL(index_start_downheelL)
    firstfoot=1;
%     fprintf('Right foot touches first\n');
    touchfirst=tdownheelR(index_start_downheelR);
else
    firstfoot=2;
%     fprintf('Left foot touches first\n');
    touchfirst=tdownheelL(index_start_downheelL);
end

%   which foot touches last?

if tdownheelR(index_stop_downheelR)>tdownheelL(index_stop_downheelL)
    lastfoot=1;
%     fprintf('Right foot touches last\n');
    touchlast=tdownheelR(index_stop_downheelR);
else
    lastfoot=2;
%     fprintf('Left foot touches last\n');
    touchlast=tdownheelL(index_stop_downheelL);
end

%   ambulation time

AmbulationTime=touchlast-touchfirst;
% fprintf('Ambulation Time %8.3f s\n',AmbulationTime);

%   stride time

%   right leg

StrideTimeRightAve=(tdownheelR(index_stop_downheelR)-tdownheelR(index_start_downheelR))/StrideCountRight;
%fprintf('Average Stride Time Right %8.3f s\n',StrideTimeRightAve);

for i=1:StrideCountRight
    StrideTimeRight(i)=tdownheelR(index_start_downheelR+i)-tdownheelR(index_start_downheelR+i-1);
end
StrideTimeRightAve=mean(StrideTimeRight);
% fprintf('Average Stride Time Right %8.3f s\n',StrideTimeRightAve);
StrideTimeRightStd=std(StrideTimeRight);
% fprintf('Standard Deviation Stride Time Right %8.3f s\n',StrideTimeRightStd);

% left leg

StrideTimeLeftAve=(tdownheelL(index_stop_downheelL)-tdownheelL(index_start_downheelL))/StrideCountLeft;
%fprintf('Average Stride Time Left %8.3f s\n',StrideTimeLeftAve);

for i=1:StrideCountLeft
    StrideTimeLeft(i)=tdownheelL(index_start_downheelL+i)-tdownheelL(index_start_downheelL+i-1);
end
StrideTimeLeftAve=mean(StrideTimeLeft);
StrideTimeLeftStd=std(StrideTimeLeft);
% fprintf('Average Stride Time Left %8.3f s\n',StrideTimeLeftAve);
% fprintf('Standard Deviation Stride Time Left%8.3f s\n',StrideTimeLeftStd);

%   swing time

%   right leg

for i=1:StrideCountRight
    SwingTimeRight(i)=tdownheelR(index_start_downheelR+i)-tuptoesR(index_start_downheelR+i-1);
end
SwingTimeRightAve=mean(SwingTimeRight);
SwingTimeRightStd=std(SwingTimeRight);
% fprintf('Average Swing Time Right %8.3f s\n',SwingTimeRightAve);
% fprintf('Standard Deviation Swing Time Right %8.3f s\n',SwingTimeRightStd);

%   left leg

for i=1:StrideCountLeft
    SwingTimeLeft(i)=tdownheelL(index_start_downheelL+i)-tuptoesL(index_start_downheelL+i-1);
end
SwingTimeLeftAve=mean(SwingTimeLeft);
SwingTimeLeftStd=std(SwingTimeLeft);
% fprintf('Average Swing Time Left %8.3f s\n',SwingTimeLeftAve);
% fprintf('Standard Deviation Swing Time Left %8.3f s\n',SwingTimeLeftStd);

%   step time

if firstfoot==1
    if lastfoot==1
        for i=1:StrideCountRight
            StepTimeRight(i)=tdownheelL(index_start_downheelL+i-1)-tdownheelR(index_start_downheelR+i-1);
            StepTimeLeft(i)=tdownheelR(index_start_downheelR+i)-tdownheelL(index_start_downheelL+i-1);
        end
    else
        for i=1:StrideCountRight-1
            StepTimeRight(i)=tdownheelL(index_start_downheelL+i-1)-tdownheelR(index_start_downheelR+i-1);
            StepTimeLeft(i)=tdownheelR(index_start_downheelR+i)-tdownheelL(index_start_downheelL+i-1);
        end
        i=StrideCountRight;
        StepTimeRight(i)=tdownheelL(index_start_downheelL+i-1)-tdownheelR(index_start_downheelR+i-1);
    end
else
    if lastfoot==2
        for i=1:StrideCountLeft
            StepTimeLeft(i)=tdownheelR(index_start_downheelR+i-1)-tdownheelL(index_start_downheelL+i-1);
            StepTimeRight(i)=tdownheelL(index_start_downheelL+i)-tdownheelR(index_start_downheelR+i-1);
        end
    else
        for i=1:StrideCountLeft-1
            StepTimeLeft(i)=tdownheelR(index_start_downheelR+i-1)-tdownheelL(index_start_downheelL+i-1);
            StepTimeRight(i)=tdownheelL(index_start_downheelL+i)-tdownheelR(index_start_downheelR+i-1);
        end
        i=StrideCountLeft;
        StepTimeLeft(i)=tdownheelR(index_start_downheelR+i-1)-tdownheelL(index_start_downheelL+i-1);
    end
end
StepTimeRightAve=mean(StepTimeRight);
% fprintf('Average Step Time Right %8.3f s\n',StepTimeRightAve);
StepTimeRightStd=std(StepTimeRight);
% fprintf('Standard Deviation Step Time Right %8.3f s\n',StepTimeRightStd);
StepTimeLeftAve=mean(StepTimeLeft);
% fprintf('Average Step Time Left %8.3f s\n',StepTimeLeftAve);
StepTimeLeftStd=std(StepTimeLeft);
% fprintf('Standard Deviation Step Time Left %8.3f s\n',StepTimeLeftStd);

%   stance time

%   right leg

for i=1:StrideCountRight
    StanceTimeRight(i)=tuptoesR(index_start_downheelR+i-1)-tdownheelR(index_start_downheelR+i-1);
end
StanceTimeRightAve=mean(StanceTimeRight);
StanceTimeRightStd=std(StanceTimeRight);
% fprintf('Average Stance Time Right %8.3f s\n',StanceTimeRightAve);
% fprintf('Standard Deviation Stance Time Right %8.3f s\n',StanceTimeRightStd);

%   left leg

for i=1:StrideCountLeft
    StanceTimeLeft(i)=tuptoesL(index_start_downheelL+i-1)-tdownheelL(index_start_downheelL+i-1);
end
StanceTimeLeftAve=mean(StanceTimeLeft);
StanceTimeLeftStd=std(StanceTimeLeft);
% fprintf('Average Stance Time Left %8.3f s\n',StanceTimeLeftAve);
% fprintf('Standard Deviation Stance Time Left %8.3f s\n',StanceTimeLeftStd);

%   double support time

if firstfoot==1
    for i=1:StrideCountRight
        DoubleSupportTimeRight(i)=tuptoesR(index_start_downheelR+i-1)-tdownheelL(index_start_downheelL+i-1);
        DoubleSupportTimeLeft(i)=tuptoesL(index_start_downheelL+i-2)-tdownheelR(index_start_downheelR+i-1);
    end
else
    for i=1:StrideCountLeft
        DoubleSupportTimeLeft(i)=tuptoesL(index_start_downheelL+i-1)-tdownheelR(index_start_downheelR+i-1);
        DoubleSupportTimeRight(i)=tuptoesR(index_start_downheelR+i-2)-tdownheelL(index_start_downheelL+i-1);
    end
end
DoubleSupportTimeRightAve=mean(DoubleSupportTimeRight);
DoubleSupportTimeLeftAve=mean(DoubleSupportTimeLeft);
DoubleSupportTimeRightStd=std(DoubleSupportTimeRight);
DoubleSupportTimeLeftStd=std(DoubleSupportTimeLeft);
% fprintf('Average Double Support Time Right %8.3f s\n',DoubleSupportTimeRightAve);
% fprintf('Standard Deviation Double Support Time Right %8.3f s\n',DoubleSupportTimeRightStd);
% fprintf('Average Double Support Time Left %8.3f s\n',DoubleSupportTimeLeftAve);
% fprintf('Standard Deviation Double Support Time Left %8.3f s\n',DoubleSupportTimeLeftStd);

%   heel off on time

%   right leg

for i=1:StrideCountRight+1
    HeelOffOnTimeRight(i)=tdowntoesR(index_start_downheelR+i-1)-tdownheelR(index_start_downheelR+i-1);
end
HeelOffOnTimeRightAve=mean(HeelOffOnTimeRight);
HeelOffOnTimeRightStd=std(HeelOffOnTimeRight);
% fprintf('Average Heel Off On Time Right %8.3f s\n',HeelOffOnTimeRightAve);
% fprintf('Standard Deviation Heel Off On Time Right %8.3f s\n',HeelOffOnTimeRightStd);

%   left leg

for i=1:StrideCountLeft+1
    HeelOffOnTimeLeft(i)=tdowntoesL(index_start_downheelL+i-1)-tdownheelL(index_start_downheelL+i-1);
end
HeelOffOnTimeLeftAve=mean(HeelOffOnTimeLeft);
HeelOffOnTimeLeftStd=std(HeelOffOnTimeLeft);
% fprintf('Average Heel Off On Time Left %8.3f s\n',HeelOffOnTimeLeftAve);
% fprintf('Standard Deviation Heel Off On Time Left %8.3f s\n',HeelOffOnTimeLeftStd);


%%%%%% SUMMARY AND STATISTICS %%%%%%%


Step_Count=StepCount;
Ambulation_Time =AmbulationTime;
Cadence=StepCount*60/AmbulationTime;
Step_Time_Differential_Av=StepTimeRightAve-StepTimeLeftAve;
Cycle_Time_Differential_Av=StrideTimeRightAve-StrideTimeLeftAve;
Step_Time_Right_Av=StepTimeRightAve;
Step_Time_Left_Av=StepTimeLeftAve;
Cycle_Time_Right_Av=StrideTimeRightAve;
Cycle_Time_Left_Av=StrideTimeLeftAve;
Swing_Time_Right_Av=SwingTimeRightAve;
Swing_Time_Left_Av=SwingTimeLeftAve;
Stance_Time_Right_Av=StanceTimeRightAve;
Stance_Time_Left_Av=StanceTimeLeftAve;
Single_Supp_Time_Right_Av=SwingTimeLeftAve;
Single_Supp_Time_Left_Av=SwingTimeRightAve;
Double_Support_Time_Right_Av=DoubleSupportTimeRightAve;
Double_Support_Time_Left_Av=DoubleSupportTimeLeftAve;
Swing_Cycle_Right_Av=SwingTimeRightAve*100/StrideTimeRightAve;
Swing_Cycle_Left_Av=SwingTimeLeftAve*100/StrideTimeLeftAve;
Stance_Cycle_Right_Av=StanceTimeRightAve*100/StrideTimeRightAve;
Stance_Cycle_Left_Av=StanceTimeLeftAve*100/StrideTimeLeftAve;
Single_Supp_Cycle_Right_Av=SwingTimeRightAve*100/StrideTimeRightAve;
Single_Supp_Cycle_Left_Av=SwingTimeLeftAve*100/StrideTimeLeftAve;
Double_Support_Cycle_Right_Av=DoubleSupportTimeRightAve*100/StrideTimeRightAve;
Double_Support_Cycle_Left_Av=DoubleSupportTimeLeftAve*100/StrideTimeLeftAve;
Heel_Off_On_Time_Right_Av=HeelOffOnTimeRightAve;
Heel_Off_On_Time_Left_Av=HeelOffOnTimeLeftAve;
Step_Time_Right_Std=StepTimeRightStd;
Step_Time_Left_Std=StepTimeLeftStd;
Stride_Time_Right_Std=StrideTimeRightStd;
Stride_Time_Left_Std=StrideTimeLeftStd;
Swing_Time_Right_Std=SwingTimeRightStd;
Swing_Time_Left_Std=SwingTimeLeftStd;
Stance_Time_Right_Std=StanceTimeRightStd;
Stance_Time_Left_Std=StanceTimeLeftStd;
Single_Support_Right_Std=SwingTimeLeftStd;
Single_Support_Left_Std=SwingTimeRightStd;
Double_Support_Time_Right_Std=DoubleSupportTimeRightStd;
Double_Support_Time_Left_Std=DoubleSupportTimeLeftStd;
Heel_Off_On_Right_Std=HeelOffOnTimeRightStd;
Heel_Off_On_Left_Std=HeelOffOnTimeLeftStd;
Heel_Off_On_Cycle_Av_L=HeelOffOnTimeLeftAve*100/StrideTimeLeftAve;
Heel_Off_On_Cycle_Av_R=HeelOffOnTimeRightAve*100/StrideTimeRightAve;

    end
end
end